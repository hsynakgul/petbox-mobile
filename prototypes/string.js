String.prototype.toUpperCaseTurkish = function () {
  let string = this
  const letters = { i: "İ", ş: "Ş", ğ: "Ğ", ü: "Ü", ö: "Ö", ç: "Ç", ı: "I" }
  string = string.replace(/(([iışğüçö]))+/g, function (letter) {
    let str = ""
    letter.toString().split("").map((s) => {
      str += letters[s]
    })
    return str
  })
  return string.toUpperCase()
}

String.prototype.toLowerCaseTurkish = function () {
  let string = this
  const letters = { İ: "i", I: "ı", Ş: "ş", Ğ: "ğ", Ü: "ü", Ö: "ö", Ç: "ç" }
  string = string.replace(/(([İIŞĞÜÇÖ]))+/g, function (letter) {
    let str = ""
    letter.toString().split("").map((s) => {
      str += letters[s]
    })
    return str
  })
  return string.toLowerCase()
}

String.prototype.toCapitalizeTurkish = function () {
  let string = this.toLowerCaseTurkish()
  return string.replace(/^.|\s\S/g, function (a) {
    return a.toUpperCase()
  })
}

String.prototype.toSlug = function (suffix = "_") {
  let string = this.toLowerCaseTurkish()
  return string
    .replace(/[^_a-zA-Z0-9\s]+/gi, "") // remove non-alphanumeric chars
    .replace(/\s/gi, suffix) // convert spaces to dashes
    .replace(/[-_]+/gi, suffix) // trim repeated dashes
}
