import React, { useEffect, useRef, useState } from "react"
import { AppState, Vibration } from "react-native"
import { observer } from "mobx-react-lite"
import { SocketProviderProps } from "./index"

import Pusher from "pusher-js/react-native"
import { useStores } from "../models"
import { Api } from "../services/api"
import { onPatch } from "mobx-state-tree"
import { Channel } from "pusher-js"
import base64 from 'react-native-base64'
// Enable pusher logging - don't include this in production
Pusher.logToConsole = true

export default observer(function PusherProvider(props: SocketProviderProps) {
  const { authStore, chatStore } = useStores()

  const [api] = useState<Api>(new Api())

  useEffect(() => {
    api.setup()
  }, [])

  const authorizer = (channel, options) => {
    return {
      authorize: (socketId, callback) => {
        api
          .pusherAuth({
            socket_id: socketId,
            channel_name: channel.name,
          })
          .then((res) => {
            if (res.kind !== "ok") {
              throw new Error(`Received: ${res.message}`)
            }
            return res.data
          })
          .then((data) => {
            callback(null, data)
          })
          .catch((err) => {
            callback(new Error(`Error calling auth endpoint: ${err}`), {
              auth: "",
            })
          })
      },
    }
  }

  const pusher = useRef<Pusher>(null)
  const channel = useRef<Channel>(null)
  const createPusher = () => {
    pusher.current = new Pusher("3ebe9edc03b07dc0ea06", {
      cluster: "eu",
      authorizer: authorizer,
      enabledTransports: ["ws", "wss"],
    })
    pusher.current.connection.bind("connected", () => {
      createPusherChannel()
    })
    pusher.current.connection.bind("disconnected", () => {
      if (channel.current) {
        channel.current.unbind()
        pusher.current.unsubscribe(channel.current.name)
      }
    })
    pusher.current.connection.bind("error", (err) => {
      console.tron.log({
        err,
      })
    })
  }

  const createPusherChannel = () => {
    if (pusher.current) {
      channel.current = pusher.current.subscribe(`private-users-${authStore.id}`)
      channel.current.bind("send-message", function (data) {
        try {
          const utf8Text = base64.decode(data.message)
          const payload = JSON.parse(utf8Text)
          chatStore.setMessage(payload)
          Vibration.vibrate()
        } catch (e) {
          console.tron.log({
            name: "error",
            e
          })
        }
      })
    }
  }

  const connectionPusher = () => {
    if (authStore.token) {
      if (pusher.current && pusher.current.connection.state === "connected") disconnectionPusher()
      createPusher()
    }
  }

  const disconnectionPusher = () => {
    if (pusher.current) {
      pusher.current.disconnect()
    }
  }

  useEffect(() => {
    connectionPusher()
    onPatch(authStore, (patch) => {
      if (patch.path === "/token") {
        if (patch.value) {
          connectionPusher()
        } else {
          disconnectionPusher()
        }
      }
    })
    AppState.addEventListener("change", (nextAppState) => {
      if (nextAppState === "inactive") {
        disconnectionPusher()
      } else if (nextAppState === "active") {
        connectionPusher()
      }
    })
  }, [])
  // endregion

  return <>{props.children}</>
})
