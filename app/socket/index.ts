import PusherProvider from './pusher-provider'

export const SocketProvider = PusherProvider

export interface SocketProviderProps {
  /**
   * Children components.
   */
  children: React.ReactNode
}
