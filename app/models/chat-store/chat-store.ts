import { applySnapshot, flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import { GetChatResult, GetChatsResult } from "../../services/api"
import { withEnvironment } from "../extensions/with-environment"
import { withRootStore } from "../extensions/with-root-store"
import R, { findIndex, propEq } from "ramda"
import { Chat as ChatApiType } from "../../services/api/api.types"

/**
 * Model description here for TypeScript hints.
 */
// export const MessageModel = types
//   .model("Message")
//   .props({
//     id: types.maybeNull(types.number),
//     chat_id: types.maybeNull(types.number),
//     user_id: types.maybeNull(types.number),
//     text: types.maybeNull(types.string),
//     created_at: types.maybeNull(types.string),
//   })
//   .views(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
//   .actions(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
 * Model description here for TypeScript hints.
 */
export const ChatModel = types
  .model("Chat")
  .props({
    id: types.number,
    users: types.array(types.frozen()),
    messages: types.array(types.frozen()),
  })
  .extend(withEnvironment)
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setMessage(payload: Record<string, any>) {
      self.messages.splice(0, 0, payload)
    },
  }))
  .actions((self) => ({
    sendMessage(payload: Record<string, any>) {
      self.environment.api
        .sendMessage({
          ...payload,
          chat_id: self.id,
        })
        .then((rsp) => {
          if (rsp.kind === "ok") {
            self.setMessage(rsp.data)
          }
        })
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type ChatType = Instance<typeof ChatModel>
export interface Chat extends ChatType {}
type ChatSnapshotType = SnapshotOut<typeof ChatModel>
export interface ChatSnapshot extends ChatSnapshotType {}

/**
 * Model description here for TypeScript hints.
 */
export const ChatStoreModel = types
  .model("ChatStore")
  .extend(withEnvironment)
  .extend(withRootStore)
  .props({
    list: types.optional(types.array(ChatModel), []),
  })
  .views((self) => ({
    get allChats(): Record<string, any>[] {
      return self.list
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    fetch: flow(function* (payload?: Record<string, any>) {
      const result: GetChatsResult = yield self.environment.api.getChats(payload)
      if (result.kind === "ok") {
        const finalTodos = R.map((chat: ChatApiType) => {
          const index = findIndex(propEq("id", chat.id), self.list)
          if (index > -1) {
            chat.messages = R.uniq(R.concat(chat.messages, self.list[index].messages.slice(0)))
          }
          return chat
        }, result.data)
        applySnapshot(self.list, finalTodos.slice(0))
      }
      return result
    }),
    createChat: flow(function* (payload?: Record<string, any>) {
      const result: GetChatResult = yield self.environment.api.createChat(payload)
      if (result.kind === "ok") {
        applySnapshot(self.list, R.uniq(R.concat([result.data], self.list.slice(0))))
      }
      return result
    }),
    setMessage: flow(function* (payload?: Record<string, any>) {
      const index = findIndex(propEq("id", payload.chat_id), self.list)
      if (index > -1) {
        self.list[index].setMessage(payload)
      }
    }),
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type ChatStoreType = Instance<typeof ChatStoreModel>
export interface ChatStore extends ChatStoreType {}
type ChatStoreSnapshotType = SnapshotOut<typeof ChatStoreModel>
export interface ChatStoreSnapshot extends ChatStoreSnapshotType {}
