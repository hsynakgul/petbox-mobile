import { ChatStoreModel } from "../chat-store/chat-store"
import { PostStoreModel } from "../post-store/post-store"
import { CategoryStoreModel } from "../category-store/category-store"
import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { ToastStoreModel } from "../toast-store/toast-store"
import { AuthStoreModel } from "../auth-store/auth-store"
import { SystemStoreModel } from "../system-store/system-store"
import { omit } from "ramda"

/**
 * A RootStore model.
 */
// prettier-ignore
export const RootStoreModel = types.model("RootStore").props({
  chatStore: types.optional(ChatStoreModel, {}),
  postStore: types.optional(PostStoreModel, {}),
  categoryStore: types.optional(CategoryStoreModel, {}),
  toastStore: types.optional(ToastStoreModel, {}),
  systemStore: types.optional(SystemStoreModel, {}),
  authStore: types.optional(AuthStoreModel, {}),
}).postProcessSnapshot(omit(["toastStore"]))

/**
 * The RootStore instance.
 */
export interface RootStore extends Instance<typeof RootStoreModel> {}

/**
 * The data of a RootStore.
 */
export interface RootStoreSnapshot extends SnapshotOut<typeof RootStoreModel> {}
