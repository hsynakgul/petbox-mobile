import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { v4 as uuidV4 } from "uuid"
import { findIndex, pathOr, propEq } from "ramda"

// export const ToastModel = types.model("Toast").props({
//   type: types.enumeration(["error", "info", "success"]),
//   uuid: types.optional(types.string, ""),
//   title: types.string,
//   text: types.optional(types.string, ""),
//   timing: types.optional(types.enumeration(["short", "normal", "long"]), "normal"),
// })
//
// type ToastType = Instance<typeof ToastModel>
// export interface Toast extends ToastType {}
// type ToastSnapshotType = SnapshotOut<typeof ToastModel>
// export interface ToastSnapshot {
//   type: "error" | "info" | "success"
//   uuid?: string
// }

export interface ToastInterface {
  type: "error" | "info" | "success"
  uuid?: string
  title: string
  text?: string
  timing?: "short" | "normal" | "long"
}

/**
 * Model description here for TypeScript hints.
 */
export const ToastStoreModel = types
  .model("ToastStore")
  .props({
    toastItems: types.optional(types.array(types.frozen()), []),
    selectedToast: types.maybeNull(types.string),
  })
  .views((self) => ({
    getToast(uuid: string) {
      const index = findIndex(propEq("uuid", uuid), self.toastItems)
      return pathOr(null, [index], self.toastItems)
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    pushToast: (toast: ToastInterface) => {
      const uuid = uuidV4()
      if (self.toastItems.length === 0) {
        self.selectedToast = uuid
      }
      self.toastItems.push({ ...toast, timing: toast.timing || "normal", uuid })
    },
    removeToast: (uuid: string) => {
      const index = findIndex(propEq("uuid", uuid), self.toastItems)
      if (index > -1) self.toastItems.splice(index, 1)
      if (self.toastItems.length > 0) self.selectedToast = self.toastItems[0].uuid
      else self.selectedToast = null
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type ToastStoreType = Instance<typeof ToastStoreModel>
export interface ToastStore extends ToastStoreType {}
type ToastStoreSnapshotType = SnapshotOut<typeof ToastStoreModel>
export interface ToastStoreSnapshot extends ToastStoreSnapshotType {}
