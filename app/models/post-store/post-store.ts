import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import { GetNoContentResult, GetPostResult, GetPostsResult } from "../../services/api"
import { withEnvironment } from "../extensions/with-environment"
import { withRootStore } from "../extensions/with-root-store"

/**
 * Model description here for TypeScript hints.
 */
export const PostStoreModel = types
  .model("PostStore")
  .extend(withEnvironment)
  .extend(withRootStore)
  .props({
    detail: types.frozen(),
    list: types.array(types.frozen()),
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    fetch: flow(function* (payload?: Record<string, any>) {
      const result: GetPostsResult = yield self.environment.api.getPosts(payload)
      if (result.kind === "ok") {
        self.list.replace(result.data)
      }
      return result
    }),
    get: flow(function* (postId: any) {
      const result: GetPostsResult = yield self.environment.api.getPost(postId)
      if (result.kind === "ok") {
        self.detail = result.data
      }
      return result
    }),
    store: flow(function* (payload: Record<string, any>) {
      const result: GetPostResult = yield self.environment.api.storePost(payload)
      if (result.kind === "ok") {
      }
      return result
    }),
    update: flow(function* (postId: any, payload: Record<string, any>) {
      const result: GetNoContentResult = yield self.environment.api.updatePost(postId, payload)
      if (result.kind === "ok") {
      }
      return result
    }),
    destroy: flow(function* (postId: any) {
      const result: GetPostResult = yield self.environment.api.destroyPost(postId)
      if (result.kind === "ok") {
      }
      return result
    }),
    changeStatus: flow(function* (postId: any, payload: Record<string, any>) {
      const result: GetNoContentResult = yield self.environment.api.changeStatusPost(postId, payload)
      if (result.kind === "ok") {
      }
      return result
    }),
    favorite: flow(function* (postId: any, value: number) {
      const result: GetNoContentResult = yield self.environment.api.favoritePost(postId, value)
      return result
    }),
    reset() {
      self.detail = {}
      self.list.replace([])
    }
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type PostStoreType = Instance<typeof PostStoreModel>
export interface PostStore extends PostStoreType {}
type PostStoreSnapshotType = SnapshotOut<typeof PostStoreModel>
export interface PostStoreSnapshot extends PostStoreSnapshotType {}
