import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import { GetCategoriesResult } from "../../services/api"
import { withEnvironment } from "../extensions/with-environment"
import { withRootStore } from "../extensions/with-root-store"

/**
 * Model description here for TypeScript hints.
 */
export const CategoryStoreModel = types
  .model("CategoryStore")
  .extend(withEnvironment)
  .extend(withRootStore)
  .props({
    list: types.optional(types.array(types.frozen()), []),
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    fetch: flow(function* () {
      const result: GetCategoriesResult = yield self.environment.api.fetchCategories()
      if (result.kind === "ok") {
        self.list.replace(result.data)
      }
      return result
    }),
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type CategoryStoreType = Instance<typeof CategoryStoreModel>
export interface CategoryStore extends CategoryStoreType {}
type CategoryStoreSnapshotType = SnapshotOut<typeof CategoryStoreModel>
export interface CategoryStoreSnapshot extends CategoryStoreSnapshotType {}
