import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { omit, pathOr } from "ramda"

/**
 * Model description here for TypeScript hints.
 */
export const SystemStoreModel = types
  .model("SystemStore")
  .props({
    intro: types.optional(types.boolean, true),
    loading: types.optional(types.boolean, true),
    deviceInfo: types.frozen(),
  })
  .views((self) => ({
    getDeviceName(): string {
      let name = pathOr("", ["deviceName"], self.deviceInfo)
      if (!name) {
        name = `${pathOr("", ["brand"], self.deviceInfo)} - ${pathOr(
          "",
          ["model"],
          self.deviceInfo,
        )}`
      }
      return name
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setIntro: (value: boolean) => {
      self.intro = value
    },
    setLoading: (value: boolean) => {
      self.loading = value
    },
    setDeviceInfo: (value: any) => {
      self.deviceInfo = value
    },
  }))
  .postProcessSnapshot(omit(["loading"])) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type SystemStoreType = Instance<typeof SystemStoreModel>
export interface SystemStore extends SystemStoreType {}
type SystemStoreSnapshotType = SnapshotOut<typeof SystemStoreModel>
export interface SystemStoreSnapshot extends SystemStoreSnapshotType {}
