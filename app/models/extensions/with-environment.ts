import { getEnv, getRoot, IStateTreeNode } from "mobx-state-tree"
import { Environment } from "../environment"
import { pathOr } from "ramda"
import { RootStoreModel } from ".."

/**
 * Adds a environment property to the node for accessing our
 * Environment in strongly typed.
 */
export const withEnvironment = (self: IStateTreeNode) => ({
  views: {
    /**
     * The environment.
     */
    get environment() {
      let rootStore = pathOr(null, ["rootStore"], self)
      if (!rootStore) rootStore = getRoot<typeof RootStoreModel>(self)
      const environment = getEnv<Environment>(self)
      environment.api.setMobX(rootStore)
      return environment
    },
  },
})
