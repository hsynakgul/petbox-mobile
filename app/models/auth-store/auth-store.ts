import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import { withEnvironment } from "../extensions/with-environment"
import { withRootStore } from "../extensions/with-root-store"
import { remove, saveString } from "../../utils/storage"
// import { omit } from "ramda"
import { GetPostsResult, GetProfileResult, LoginResult, Profile } from "../../services/api"

/**
 * Model description here for TypeScript hints.
 */
export const AuthStoreModel = types
  .model("AuthStore")
  .extend(withEnvironment)
  .extend(withRootStore)
  .props({
    token: types.optional(types.string, ""),
    id: types.optional(types.number, 0),
    profile: types.frozen(),
    posts: types.optional(types.frozen(), { data: [], meta: {} }),
    favorites: types.array(types.frozen()),
  })
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    login: flow(function* (email: string, password: string) {
      yield remove("token")
      const result: LoginResult = yield self.environment.api.login(
        email,
        password,
        self.rootStore.systemStore.getDeviceName(),
      )
      if (result.kind === "ok") {
        yield saveString("token", result.token)
        self.profile = result.data
        self.id = result.data.id
        self.token = result.token
      } else {
        self.rootStore.toastStore.pushToast({
          title: "Giriş Bilgileri Hatalı!",
          text: result.message,
          type: "error",
        })
      }
      return result
    }),
    register: flow(function* (params: object) {
      yield remove("token")
      const result: LoginResult = yield self.environment.api.register({
        ...params,
        device_name: self.rootStore.systemStore.getDeviceName(),
      })
      if (result.kind === "ok") {
        yield saveString("token", result.token)
        self.profile = result.data
        self.id = result.data.id
        self.token = result.token
      } else {
        self.rootStore.toastStore.pushToast({
          title: "Oops!",
          text: result.message,
          type: "error",
        })
      }
      return result
    }),
    logout: flow(function* () {
      yield remove("token")
      self.token = ""
      self.rootStore.systemStore.setLoading(true)
      self.rootStore.postStore.reset()
      self.rootStore.authStore.reset()
    }),
    getProfile: flow(function* () {
      const result: GetProfileResult = yield self.environment.api.getProfile()
      if (result.kind === "ok") {
        self.profile = result.data
        self.id = result.data.id
      }
      return result
    }),
    update: flow(function* (params: Profile) {
      const result: GetProfileResult = yield self.environment.api.updateProfile(params)
      if (result.kind === "ok") {
        self.profile = result.data
      }
      return result
    }),
    getPosts: flow(function* () {
      const result: GetPostsResult = yield self.environment.api.getPosts({
        user_id: self.profile.id,
        get_passive: true,
      })
      if (result.kind === "ok") {
        self.posts = {
          data: result.data,
          meta: result.meta,
        }
      }
      return result
    }),
    getFavorites: flow(function* () {
      const result: GetPostsResult = yield self.environment.api.getFavorites({
        user_id: self.profile.id,
      })
      if (result.kind === "ok") {
        self.favorites.replace(result.data)
      }
      return result
    }),
    reset() {
      self.id = 0
      self.profile = {}
      self.posts = { data: [], meta: {} }
      self.favorites.replace([])
    },
  }))
  // .postProcessSnapshot(omit(["profile"]))

/**
 * Un-comment the following to omit model attributes from your snapshots (and from async storage).
 * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

 * Note that you'll need to import `omit` from ramda, which is already included in the project!
 *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
 */

type AuthStoreType = Instance<typeof AuthStoreModel>

export interface AuthStore extends AuthStoreType {}

type AuthStoreSnapshotType = SnapshotOut<typeof AuthStoreModel>

export interface AuthStoreSnapshot extends AuthStoreSnapshotType {}
