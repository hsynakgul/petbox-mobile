import * as Types from "../api.types"
import { ApiResponse } from "apisauce"
import { getGeneralApiProblem } from "../api-problem"
import { pathOr } from "ramda"

export async function fetchCategories(this: any): Promise<Types.GetCategoriesResult> {
  const response: ApiResponse<any> = await this.apisauce.get(`/categories`)

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return { kind: "ok", data: pathOr(null, ["data", "data"], response) }
}
