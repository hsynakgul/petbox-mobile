import { ApisauceInstance, create, ApiResponse } from "apisauce"
import { getGeneralApiProblem } from "./api-problem"
import { ApiConfig, DEFAULT_API_CONFIG } from "./api-config"
import * as Types from "./api.types"
import * as auth from "./auth"
import * as category from "./category"
import * as post from "./post"
import * as file from "./file"
import * as chat from "./chat"
import { loadString } from "../../utils/storage"
import { Profile } from "./api.types"
import { RootStore } from "../../models"

/**
 * Manages all requests to the API.
 */
export class Api {
  /**
   * The underlying apisauce instance which performs the requests.
   */
  apisauce: ApisauceInstance

  /**
   * Configurable options.
   */
  config: ApiConfig

  mobX: RootStore

  /**
   * Creates the api.
   *
   * @param config The configuration to use.
   */
  constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
    this.config = config
  }

  /**
   * Sets up the API.  This will be called during the bootup
   * sequence and will happen before the first React component
   * is mounted.
   *
   * Be as quick as possible in here.
   */
  setup() {
    // construct the apisauce instance
    this.apisauce = create({
      baseURL: this.config.url,
      timeout: this.config.timeout,
      headers: {
        Accept: "application/json",
      },
    })

    this.apisauce.addAsyncRequestTransform((request) => async () => {
      const token = await loadString("token")
      if (token) request.headers["Authorization"] = `Bearer ${token}`
    })

    this.apisauce.addAsyncResponseTransform((response) => async () => {
      if (response.status === 401) {
        if (this.mobX) {
          this.mobX.authStore.logout()
        }
      }
    })
  }

  setMobX(mobX: RootStore) {
    this.mobX = mobX
  }

  /**
   * Auth
   */
  async login(email: string, password: string, deviceName: string): Promise<Types.LoginResult> {
    return auth.login.apply(this, arguments)
  }
  async register(params: object): Promise<Types.LoginResult> {
    return auth.register.apply(this, arguments)
  }
  async getProfile(): Promise<Types.GetProfileResult> {
    return auth.getProfile.apply(this, arguments)
  }
  async updateProfile(query: Profile): Promise<Types.GetProfileResult> {
    return auth.updateProfile.apply(this, arguments)
  }
  async pusherAuth(payload: Record<string, any>): Promise<Types.GetPusherAuthResult> {
    return auth.pusherAuth.apply(this, arguments)
  }

  /**
   * Category
   */
  async fetchCategories(): Promise<Types.GetCategoriesResult> {
    return category.fetchCategories.apply(this, arguments)
  }

  /**
   * Post
   */
  async getPost(id: any): Promise<Types.GetPostResult> {
    return post.getPost.apply(this, arguments)
  }

  async getPosts(payload?: Record<string, any>): Promise<Types.GetPostsResult> {
    return post.getPosts.apply(this, arguments)
  }

  async getFavorites(payload?: Record<string, any>): Promise<Types.GetPostsResult> {
    return post.getFavorites.apply(this, arguments)
  }

  async storePost(payload?: Record<string, any>): Promise<Types.GetPostResult> {
    return post.storePost.apply(this, arguments)
  }

  async updatePost(postId: any, payload?: Record<string, any>): Promise<Types.GetPostResult> {
    return post.updatePost.apply(this, arguments)
  }

  async destroyPost(postId: any, payload?: Record<string, any>): Promise<Types.GetNoContentResult> {
    return post.destroyPost.apply(this, arguments)
  }

  async changeStatusPost(postId: any, payload?: Record<string, any>): Promise<Types.GetNoContentResult> {
    return post.changeStatusPost.apply(this, arguments)
  }

  async favoritePost(postId: any, value: number): Promise<Types.GetNoContentResult> {
    return post.favoritePost.apply(this, arguments)
  }

  /**
   * Chats
   */
  async getChats(payload?: Record<string, any>): Promise<Types.GetChatsResult> {
    return chat.getChats.apply(this, arguments)
  }

  async createChat(payload?: Record<string, any>): Promise<Types.GetChatResult> {
    return chat.createChat.apply(this, arguments)
  }

  async sendMessage(payload?: Record<string, any>): Promise<Types.GetChatMessageResult> {
    return chat.sendMessage.apply(this, arguments)
  }

  /**
   * File
   */
  async storeFile(params: object): Promise<Types.GetFileResult> {
    return file.storeFile.apply(this, arguments)
  }

  /**
   * Gets a list of users.
   */
  async getUsers(): Promise<Types.GetUsersResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(`/users`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const convertUser = (raw) => {
      return {
        id: raw.id,
        name: raw.name,
      }
    }

    // transform the data into the format we are expecting
    try {
      const rawUsers = response.data
      const resultUsers: Types.User[] = rawUsers.map(convertUser)
      return { kind: "ok", users: resultUsers }
    } catch {
      return { kind: "bad-data" }
    }
  }

  /**
   * Gets a single user by ID
   */

  async getUser(id: string): Promise<Types.GetUserResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(`/users/${id}`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    // transform the data into the format we are expecting
    try {
      const resultUser: Types.User = {
        id: response.data.id,
        name: response.data.name,
      }
      return { kind: "ok", user: resultUser }
    } catch {
      return { kind: "bad-data" }
    }
  }
}
