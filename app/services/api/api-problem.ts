import { ApiResponse } from "apisauce"
import { pathOr } from "ramda"

export type GeneralApiProblem =
/**
 * Times up.
 */
  | { kind: "timeout"; message?: string | null; temporary: true }
  /**
   * Cannot connect to the server for some reason.
   */
  | { kind: "cannot-connect"; message?: string | null; temporary: true }
  /**
   * The server experienced a problem. Any 5xx error.
   */
  | { kind: "server"; message: string | null }
  /**
   * We're not allowed because we haven't identified ourself. This is 401.
   */
  | { kind: "unauthorized"; message: string | null }
  /**
   * We don't have access to perform that request. This is 403.
   */
  | { kind: "forbidden"; message: string | null }
  /**
   * Unable to find that resource.  This is a 404.
   */
  | { kind: "not-found"; message: string | null }
  /**
   *  This is a 422.
   */
  | { kind: "validation"; message: string | null; validation: object | null }
  /**
   * All other 4xx series errors.
   */
  | { kind: "rejected"; message: string | null }
  /**
   * Something truly unexpected happened. Most likely can try again. This is a catch all.
   */
  | { kind: "unknown"; temporary: true; message?: string | null}
  /**
   * The data we received is not in the expected format.
   */
  | { kind: "bad-data"; message?: string | null }

/**
 * Attempts to get a common cause of problems from an api response.
 *
 * @param response The api response.
 */
export function getGeneralApiProblem(response: ApiResponse<any>): GeneralApiProblem | void {
  switch (response.problem) {
    case "CONNECTION_ERROR":
      return { kind: "cannot-connect", temporary: true }
    case "NETWORK_ERROR":
      return { kind: "cannot-connect", temporary: true }
    case "TIMEOUT_ERROR":
      return { kind: "timeout", temporary: true }
    case "SERVER_ERROR":
      return { kind: "server", message: pathOr("", ["data", "message"], response) }
    case "UNKNOWN_ERROR":
      return { kind: "unknown", temporary: true }
    case "CLIENT_ERROR":
      switch (response.status) {
        case 401:
          return {
            kind: "unauthorized",
            message: pathOr("", ["data", "error", "message"], response),
          }
        case 403:
          return { kind: "forbidden", message: pathOr("", ["data", "message"], response) }
        case 404:
          return { kind: "not-found", message: pathOr("", ["data", "message"], response) }
        case 422:
          return {
            kind: "validation",
            message: pathOr("", ["data", "message"], response),
            validation: pathOr({}, ["data", "errors"], response),
          }
        default:
          return { kind: "rejected", message: pathOr("", ["data", "message"], response) }
      }
    case "CANCEL_ERROR":
      return null
  }

  return null
}
