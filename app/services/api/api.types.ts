import { GeneralApiProblem } from "./api-problem"

export interface Profile {
  id?: number
  name?: string
  email?: string
  phone?: string
  gender?: number
  email_is_valid?: number
  send_email?: number
  send_sms?: number
  send_notification?: number
}

export interface Category {
  id: number
  title: string
}

export interface Post {
  id?: number
  user_id?: number
  title?: string,
  description?: string,
  age?: number,
  sex?: number,
  category_id?: number,
  breed_id?: number,
  favorite?: number,
  status?: number,
  city?: string,
  district?: string,
  photos?: File[],
  created_at?: string
}

export interface File {
  id: any
  name?: string
  avatar?: string
  thumb?: string
  small?: string
  large?: string
}

export interface Chat {
  id: number
  users: User[],
  messages: ChatMessage[]
}

export interface ChatMessage {
  id: number
  chat_id: number
  user_id: number
  text: string
  created_at: string
  created_at_text: string
}

export interface User {
  id: number
  name: string
}

export interface PusherAuth {
  auth: string
  channel_data: Record<string, any>
}

export type GetNoContentResult = { kind: "ok"; } | GeneralApiProblem

export type LoginResult = { kind: "ok"; token: string, data: Profile } | GeneralApiProblem
export type GetProfileResult = { kind: "ok"; data: Profile } | GeneralApiProblem

export type GetCategoriesResult = { kind: "ok"; data: Category[] } | GeneralApiProblem

export type GetPostsResult = { kind: "ok"; data: Post[], meta?: Record<string, any> } | GeneralApiProblem
export type GetPostResult = { kind: "ok"; data: Post } | GeneralApiProblem

export type GetFilesResult = { kind: "ok"; data: File[] } | GeneralApiProblem
export type GetFileResult = { kind: "ok"; data: File } | GeneralApiProblem

export type GetUsersResult = { kind: "ok"; users: User[] } | GeneralApiProblem
export type GetUserResult = { kind: "ok"; user: User } | GeneralApiProblem

export type GetPusherAuthResult = { kind: "ok"; data: PusherAuth } | GeneralApiProblem

export type GetChatsResult = { kind: "ok"; data: Chat[] } | GeneralApiProblem
export type GetChatResult = { kind: "ok"; data: Chat } | GeneralApiProblem

export type GetChatMessagesResult = { kind: "ok"; data: ChatMessage[] } | GeneralApiProblem
export type GetChatMessageResult = { kind: "ok"; data: ChatMessage } | GeneralApiProblem
