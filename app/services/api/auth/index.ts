import * as Types from "../api.types"
import { ApiResponse } from "apisauce"
import { getGeneralApiProblem } from "../api-problem"
import { pathOr } from "ramda"
import { Profile } from "../api.types"

export async function login(
  this: any,
  email: string,
  password: string,
  deviceName: string,
): Promise<Types.LoginResult> {
  const response: ApiResponse<any> = await this.apisauce.post(`/auth/login`, {
    email,
    password,
    device_name: deviceName,
  })

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return {
    kind: "ok",
    data: pathOr(null, ["data", "data"], response),
    token: pathOr(null, ["data", "meta", "token"], response),
  }
}

export async function register(this: any, params: object): Promise<Types.LoginResult> {
  const response: ApiResponse<any> = await this.apisauce.post(`/auth/register`, params)

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return {
    kind: "ok",
    data: pathOr(null, ["data", "data"], response),
    token: pathOr(null, ["data", "meta", "token"], response),
  }
}

export async function getProfile(this: any): Promise<Types.GetProfileResult> {
  const response: ApiResponse<any> = await this.apisauce.get(`/auth/me`)

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return { kind: "ok", data: pathOr(null, ["data", "data"], response) }
}

export async function updateProfile(this: any, params: Profile): Promise<Types.GetProfileResult> {
  const response: ApiResponse<any> = await this.apisauce.post(`/auth/update`, params)

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return { kind: "ok", data: pathOr(null, ["data", "data"], response) }
}

export async function pusherAuth(
  this: any,
  payload: Record<string, any>,
): Promise<Types.GetPusherAuthResult> {
  const response: ApiResponse<any> = await this.apisauce.post(`/auth/pusher`, payload)

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return { kind: "ok", data: pathOr(null, ["data"], response) }
}
