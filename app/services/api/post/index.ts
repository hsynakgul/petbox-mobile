import * as Types from "../api.types"
import { ApiResponse } from "apisauce"
import { getGeneralApiProblem } from "../api-problem"
import { pathOr } from "ramda"

export async function getPost(this: any, id: any): Promise<Types.GetPostResult> {
  const response: ApiResponse<any> = await this.apisauce.get(`/posts/` + id)

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return { kind: "ok", data: pathOr(null, ["data", "data"], response) }
}

export async function getPosts(
  this: any,
  payload?: Record<string, any>,
): Promise<Types.GetPostsResult> {
  const response: ApiResponse<any> = await this.apisauce.get(`/posts`, payload || {})

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return {
    kind: "ok",
    data: pathOr(null, ["data", "data"], response),
    meta: pathOr(null, ["data", "meta"], response),
  }
}

export async function getFavorites(
  this: any,
  payload?: Record<string, any>,
): Promise<Types.GetPostsResult> {
  const response: ApiResponse<any> = await this.apisauce.get(`/posts/favorites`, payload || {})

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return {
    kind: "ok",
    data: pathOr(null, ["data", "data"], response),
  }
}

export async function storePost(
  this: any,
  payload: Record<string, any>,
): Promise<Types.GetPostResult> {
  const response: ApiResponse<any> = await this.apisauce.post(`/posts`, payload)

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return { kind: "ok", data: pathOr(null, ["data", "data"], response) }
}

export async function updatePost(
  this: any,
  postId: any,
  payload: Record<string, any>,
): Promise<Types.GetPostResult> {
  const response: ApiResponse<any> = await this.apisauce.put(`/posts/${postId}`, payload)

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return { kind: "ok", data: pathOr(null, ["data", "data"], response) }
}

export async function destroyPost(this: any, postId: any): Promise<Types.GetNoContentResult> {
  const response: ApiResponse<any> = await this.apisauce.delete(`/posts/${postId}`)

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return { kind: "ok" }
}

export async function changeStatusPost(
  this: any,
  postId: any,
  payload: Record<string, any>,
): Promise<Types.GetNoContentResult> {
  const response: ApiResponse<any> = await this.apisauce.post(`/posts/status/${postId}`, payload)

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return { kind: "ok" }
}

export async function favoritePost(
  this: any,
  postId: any,
  value: number,
): Promise<Types.GetNoContentResult> {
  const response: ApiResponse<any> = await this.apisauce.post(`/posts/favorite/${postId}`, {
    value,
  })

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return { kind: "ok" }
}
