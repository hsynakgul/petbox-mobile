import * as Types from "../api.types"
import { ApiResponse } from "apisauce"
import { getGeneralApiProblem } from "../api-problem"
import { pathOr } from "ramda"

export async function storeFile(this: any, params: object): Promise<Types.GetPostResult> {

  const form = new FormData()
  form.append('file', {
    name: pathOr("", ["filename"], params),
    uri: pathOr("", ["uri"], params),
    type: pathOr("", ["type"], params),
    width: pathOr("", ["width"], params),
    height: pathOr("", ["height"], params),
    size: pathOr("", ["size"], params),
  })

  form.append("name", pathOr("", ["filename"], params))

  console.tron.log(form)

  const response: ApiResponse<any> = await this.apisauce.post(`/files`, form, {
    headers: { "Content-Type": "multipart/form-data" },
  })

  // // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return { kind: "ok", data: pathOr(null, ["data", "data"], response) }
}
