import * as Types from "../api.types"
import { ApiResponse } from "apisauce"
import { getGeneralApiProblem } from "../api-problem"
import { pathOr } from "ramda"

export async function getChats(
  this: any,
  payload?: Record<string, any>,
): Promise<Types.GetChatsResult> {
  const response: ApiResponse<any> = await this.apisauce.get(`/chats`, payload || {})

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return {
    kind: "ok",
    data: pathOr(null, ["data", "data"], response)
  }
}

export async function createChat(
  this: any,
  payload?: Record<string, any>,
): Promise<Types.GetChatResult> {
  const response: ApiResponse<any> = await this.apisauce.post(`/chats`, payload || {})

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return {
    kind: "ok",
    data: pathOr(null, ["data", "data"], response)
  }
}

export async function sendMessage(
  this: any,
  payload?: Record<string, any>,
): Promise<Types.GetChatMessageResult> {
  const response: ApiResponse<any> = await this.apisauce.post(`/chats/messages`, payload || {})

  // the typical ways to die when calling an api
  if (!response.ok) {
    const problem = getGeneralApiProblem(response)
    if (problem) return problem
  }

  return {
    kind: "ok",
    data: pathOr(null, ["data", "data"], response)
  }
}
