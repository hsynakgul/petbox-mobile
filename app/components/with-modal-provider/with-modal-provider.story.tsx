import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { color } from "../../theme"
import { WithModalProvider } from "./with-modal-provider"

storiesOf("WithModalProvider", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <WithModalProvider style={{ backgroundColor: color.error }} />
      </UseCase>
    </Story>
  ))
