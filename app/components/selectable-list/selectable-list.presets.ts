import { TextStyle } from "react-native"
import { sizes } from "../../theme/sizes"
import { color, spacing, typography } from "../../theme"

export const INPUT_PRESETS: {
  [name: string]: TextStyle
} = {
  default: {
    fontFamily: typography.primary,
    color: color.text,
    minHeight: 28,
    fontSize: sizes.mediumPlus,
    textAlignVertical: "bottom",
    padding: 0,
  },
  horizontal: {
    flex: 1,
    fontFamily: typography.primary,
    color: color.text,
    fontSize: sizes.header,
    paddingLeft: spacing[3],
    paddingRight: spacing[5]
  },
}
