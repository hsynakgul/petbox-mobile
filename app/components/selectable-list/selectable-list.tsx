import * as React from "react"
import { Keyboard, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { color, spacing } from "../../theme"
import {
  Text,
  TextFieldAppendIcon,
  TextFieldErrors,
  TextFieldLabel,
  TextFieldPrependIcon,
} from "../"
import { sizes } from "../../theme/sizes"
import { findIndex, isEmpty, isNil, pathOr, propEq } from "ramda"
import { useCallback, useMemo, useRef, useState } from "react"
import { BottomSheetBackdrop, BottomSheetFlatList, BottomSheetModal } from "@gorhom/bottom-sheet"
import { INPUT_PRESETS } from "./selectable-list.presets"
import { CONTAINER_PRESETS, CONTENT_PRESETS } from "../text-field/text-field.presets"
import { useSafeArea } from "react-native-safe-area-context"
import { BottomSheetFlatListType } from "@gorhom/bottom-sheet/lib/typescript/components/flatList/types"

const SELECTABLE_ITEM: ViewStyle = {
  paddingVertical: spacing[4],
  paddingHorizontal: spacing[5],
  borderBottomWidth: 1,
  borderColor: color.borderLight,
  flexDirection: "row",
  alignItems: "center",
}

const SELECTABLE_ITEM_BADGE: ViewStyle = {
  width: 14,
  height: 14,
  borderColor: color.borderLight,
  backgroundColor: color.palette.lighterGrey,
  borderWidth: 2,
  borderRadius: 12,
  marginRight: spacing[3],
}

const SELECTABLE_ITEM_TEXT: TextStyle = {
  fontSize: sizes.mediumPlus,
}

export interface SelectableListProps {
  /**
   * The placeholder i18n key.
   */
  placeholderTx?: string

  /**
   * The Placeholder text if no placeholderTx is provided.
   */
  placeholder?: string

  /**
   * The label i18n key.
   */
  labelTx?: string

  /**
   * The label text if no labelTx is provided.
   */
  label?: string

  prependIcon?: string

  appendIcon?: string

  /**
   * Optional container style overrides useful for margins & padding.
   */
  style?: ViewStyle | ViewStyle[]

  /**
   * Optional style overrides for the input.
   */
  inputStyle?: TextStyle | TextStyle[]

  /**
   * Various look & feels.
   */
  preset?: "default" | "horizontal"

  forwardedRef?: any

  errors?: string[]

  hideErrorDetail?: boolean

  value?: string | number

  options: { value: string | number; text: string }[]

  onChange?(val: string | number): void
}

/**
 * Describe your component here
 */
export function SelectableList(props: SelectableListProps) {
  const {
    placeholderTx,
    placeholder,
    labelTx,
    label,
    preset = "default",
    style: styleOverride,
    inputStyle: inputStyleOverride,
    prependIcon = null,
    appendIcon = null,
    errors = [],
    hideErrorDetail = false,
    options = [],
    value = null,
    onChange = () => {},
  } = props

  const { bottom: SafeAreaBottom } = useSafeArea()

  const bottomSheetModalRef = useRef<BottomSheetModal>(null)
  const bottomSheetFlatListRef = useRef<BottomSheetFlatListType>(null)

  const snapPoints = useMemo(() => ["25%", "75%"], [])

  let containerStyle: ViewStyle = { ...CONTAINER_PRESETS[preset], ...styleOverride }
  let inputContentStyle: TextStyle = { ...CONTENT_PRESETS[preset] }
  let inputStyle: TextStyle = { ...INPUT_PRESETS[preset], ...inputStyleOverride }

  const hasLabel = !!(labelTx || label)

  if (hasLabel && preset !== "horizontal") {
    containerStyle = {
      ...containerStyle,
      height: 70,
      paddingTop: spacing[1],
      paddingHorizontal: spacing[2],
    }
  }

  // region functions
  // endregion

  // region renderFunctions
  const renderValueOrPlaceholder = useCallback(() => {
    const pTX = placeholderTx ? placeholderTx : !placeholder ? "common.choose" : ""
    if (isNil(value) || isEmpty(value)) {
      return (
        <Text
          style={[inputStyle, { color: color.palette.lighterGrey }]}
          text={placeholder}
          tx={pTX}
        />
      )
    }
    let index = findIndex(propEq("value", value), options)
    if (index === -1) {
      // @ts-ignore
      index = findIndex(propEq("value", value.toString()), options)
    }
    const text = pathOr("", [index, "text"], options)
    return <Text style={[inputStyle]} text={text} tx={text} />
  }, [value])

  const renderSelectableItem = useCallback(
    ({ item }) => {
      const isActive = value && item.value.toString() === value.toString()
      const badgeStyle = [SELECTABLE_ITEM_BADGE]
      if (isActive) badgeStyle.push({ backgroundColor: color.primary })
      const onSelect = () => {
        onChange(pathOr(null, ["value"], item))
        handleDismissPress()
      }
      return (
        <TouchableOpacity style={SELECTABLE_ITEM} onPress={onSelect} key={item.value}>
          <View style={badgeStyle} />
          <Text tx={item.text} text={item.text} style={SELECTABLE_ITEM_TEXT} />
        </TouchableOpacity>
      )
    },
    [value],
  )
  // endregion

  const selectedIndex = useMemo(() => findIndex(propEq("value", value), options), [value])

  const handlePresentPress = useCallback(() => {
    bottomSheetModalRef.current.present()
    Keyboard.dismiss()
  }, [selectedIndex])

  const handleDismissPress = useCallback(() => {
    bottomSheetModalRef.current?.dismiss()
  }, [])

  const handleChange = useCallback(
    (index: number) => {
      setActiveSnapPoint(index)
      if (selectedIndex > -1 && index === 1) {
        try {
          bottomSheetFlatListRef.current.scrollToOffset({
            animated: false,
            offset: scrollToPosition,
          })
          // bottomSheetFlatListRef.current?.scrollToIndex({ index: selectedIndex, animated: false })
        } catch (e) {}
      }
    },
    [selectedIndex],
  )

  const [activeSnapPoint, setActiveSnapPoint] = useState<number>(0)
  const [scrollToPosition, setScrollToPosition] = useState<number>(0)
  const handleScroll = useCallback((event) => {
    setScrollToPosition(event.nativeEvent.contentOffset.y)
  }, [])

  return (
    <View style={styleOverride}>
      <TouchableOpacity style={containerStyle} onPress={handlePresentPress}>
        <TextFieldPrependIcon icon={prependIcon} hasError={errors.length > 0} />
        <View style={inputContentStyle}>
          <TextFieldLabel tx={labelTx} text={label} hasError={errors.length > 0} preset={preset} />
          {renderValueOrPlaceholder()}
        </View>
        <TextFieldAppendIcon icon={appendIcon} hasError={errors.length > 0} />
      </TouchableOpacity>
      <TextFieldErrors errors={errors} hideErrorDetail={hideErrorDetail} />
      <BottomSheetModal
        ref={bottomSheetModalRef}
        snapPoints={snapPoints}
        backdropComponent={BottomSheetBackdrop}
        onChange={handleChange}
        index={selectedIndex > -1 ? activeSnapPoint : 0}
      >
        <BottomSheetFlatList
          ref={bottomSheetFlatListRef}
          data={options}
          keyExtractor={(i) => i.text}
          renderItem={renderSelectableItem}
          contentContainerStyle={{ paddingBottom: SafeAreaBottom || 0 }}
          initialNumToRender={options.length}
          onScroll={handleScroll}
        />
      </BottomSheetModal>
    </View>
  )
}
