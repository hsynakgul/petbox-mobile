import * as React from "react"
import { TextStyle, TouchableOpacity, TouchableOpacityProps, View, ViewStyle } from "react-native"
import { color, spacing } from "../../theme"
import { Modal, ModalOpenProps, ModalRefProps, Text } from "../"
import { forwardRef, RefObject, useImperativeHandle, useRef, useState } from "react"
import { sizes } from "../../theme/sizes"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const BUTTONS: ViewStyle = {}

const BUTTON: ViewStyle = {
  borderTopWidth: 1,
  borderColor: "rgba(0,0,0,0.05)",
  paddingVertical: spacing[4],
  paddingHorizontal: spacing[5],
}

const BUTTON_TEXT: TextStyle = {
  textAlign: "center",
  fontSize: sizes.medium,
}

const TITLE: TextStyle = {
  color: color.palette.blue,
  fontSize: sizes.mediumPlus,
  textAlign: "center",
  padding: spacing[5],
}

export interface AlertProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
}

export interface AlertRefProps {
  open(props?: AlertOpenProps): void
  close(): void
}

export interface AlertOpenProps extends ModalOpenProps {
  title?: string
  titleTx?: string
  text?: string
  tx?: string
  buttons?: AlertButtonProps[]
  noClose?: boolean
}

/**
 * Describe your component here
 */
export const Alert = forwardRef((props: AlertProps, ref: RefObject<AlertRefProps>) => {
  const modalRef = useRef<ModalRefProps>(null)
  const [title, setTitle] = useState<string>("")
  const [text, setText] = useState<string>("")
  const [titleTx, setTitleTx] = useState<string>("")
  const [tx, setTx] = useState<string>("")
  const [buttons, setButtons] = useState<AlertButtonProps[]>([])

  const close = () => {
    modalRef.current.close()
  }

  useImperativeHandle(ref, () => ({
    open(props: AlertOpenProps = {}) {
      const { buttons = [] } = props
      setButtons(buttons)
      setTitle(props.title || "")
      setTitleTx(props.titleTx || "")
      setText(props.text || "")
      setTx(props.tx || "")
      modalRef.current.open({ display: "center", ...props })
    },
    close: close,
  }))

  const renderButtons = () => {
    return (
      <View style={BUTTONS}>
        {buttons.map((item, index) => (
          <AlertButton key={`alert-button-${index}`} {...item} alertRef={ref} />
        ))}
      </View>
    )
  }
  return (
    <Modal ref={modalRef}>
      <View style={CONTAINER}>
        {titleTx || title ? <Text style={TITLE} text={title} tx={titleTx} /> : null}
        {tx || text ? <Text text={text} tx={tx} /> : null}
        {renderButtons()}
      </View>
    </Modal>
  )
})

export interface AlertButtonProps extends TouchableOpacityProps {
  alertRef?: RefObject<AlertRefProps>
  textStyle?: TextStyle
  tx?: string
  text?: string
  noClose?: boolean
}

export function AlertButton(props: AlertButtonProps) {
  const { style = {}, textStyle = {}, tx, text, noClose = false, ...touchableProps } = props
  const onPress = (event) => {
    if (typeof props.alertRef.current !== "undefined" && noClose === false ) props.alertRef.current.close()
    if (typeof props.onPress === "function") props.onPress(event)
  }
  return (
    <TouchableOpacity {...touchableProps} style={[BUTTON, style]} onPress={onPress}>
      <Text style={[BUTTON_TEXT, textStyle]} text={text} tx={tx} />
    </TouchableOpacity>
  )
}
