import * as React from "react"
import { TextStyle, TouchableOpacity, View, ViewStyle, Animated } from "react-native"
import { observer } from "mobx-react-lite"
import { widthPercentageToDP as wp } from "react-native-responsive-screen"
import { color, spacing, typography } from "../../theme"
import { Text } from "../"
import { ToastInterface, useStores } from "../../models"
import { memo, useEffect, useState } from "react"
import { Icon } from "../../utils/icons"
import { sizes } from "../../theme/sizes"

const CONTAINER: ViewStyle = {
  position: "absolute",
  left: 0,
  right: 0,
  top: 0,
  transform: [{ translateY: 75 }],
  zIndex: 101,
  alignItems: "center",
}

const ITEM: ViewStyle = {
  width: wp("75%"),
  minHeight: 30,
  backgroundColor: color.palette.pink,
  paddingHorizontal: spacing[4],
  paddingVertical: spacing[2],
  borderRadius: 15,
  marginBottom: spacing[3],
}

const TITLE_CONTENT: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "space-between",
  marginBottom: spacing[2],
}

const BUTTON: ViewStyle = {
  height: sizes.large,
  width: sizes.large,
  alignItems: "center",
  justifyContent: "center",
}

const COUNTER: TextStyle = {
  fontFamily: typography.primary,
  fontSize: sizes.small,
  fontWeight: "500",
  color: color.palette.white,
  height: 24,
  width: 24,
  borderRadius: 12,
  borderWidth: 1,
  borderColor: color.palette.white,
  textAlign: "center",
  paddingTop: 5,
  marginRight: spacing[2],
}

const TITLE: TextStyle = {
  fontFamily: typography.primary,
  fontSize: sizes.medium,
  fontWeight: "500",
  color: color.palette.white,
  flex: 1,
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: sizes.medium,
  color: color.palette.white,
}

/**
 * Describe your component here
 */
export const ToastMessage = memo(
  observer(function ToastMessage() {
    const { toastStore } = useStores()
    const [toast, setToast] = useState<ToastInterface>(null)
    const [counter, setCounter] = useState<number>(0)
    const [intervalFunc, setIntervalFunc] = useState(null)
    const [containerY, setContainerY] = useState(new Animated.Value(115))
    const times = {
      short: 2,
      normal: 4,
      long: 6,
    }

    const close = () => {
      setCounter(0)
    }

    const timeIntervalFunc = (second) => {
      let secondCounter = second
      setIntervalFunc(
        setInterval(() => {
          setCounter(--secondCounter)
        }, 1000),
      )
    }

    useEffect(() => {
      if (toast) {
        setCounter(times[toast.timing])
        timeIntervalFunc(times[toast.timing])
        Animated.timing(containerY, {
          duration: 200,
          toValue: 75,
          useNativeDriver: true,
          delay: 200,
        }).start()
      }
    }, [toast])

    useEffect(() => {
      if (counter <= 1) {
        if (intervalFunc) {
          clearInterval(intervalFunc)
          setIntervalFunc(null)
        }
        if (toast) toastStore.removeToast(toast.uuid)
      }
    }, [counter])

    useEffect(() => {
      setContainerY(new Animated.Value(115))
      setToast(toastStore.getToast(toastStore.selectedToast))
    }, [toastStore.selectedToast])

    if (!toast) return null

    let bgColor = color.palette.pink
    if (toast.type === "info") bgColor = color.palette.orange
    if (toast.type === "success") bgColor = color.palette.green
    return (
      <Animated.View style={[CONTAINER, { transform: [{ translateY: containerY }] }]}>
        <View style={[ITEM, { backgroundColor: bgColor }]}>
          <View style={TITLE_CONTENT}>
            <Text text={counter < 1 ? "" : counter.toString()} style={COUNTER} />
            <Text tx={toast.title} style={TITLE} />
            <TouchableOpacity style={BUTTON} onPress={() => close()}>
              <Icon name="close-square" size={sizes.large} color={color.palette.white} />
            </TouchableOpacity>
          </View>
          <Text tx={toast.text} style={TEXT} />
        </View>
      </Animated.View>
    )
  }),
)
