import * as React from "react"
import {
  Animated,
  KeyboardAvoidingView,
  Platform,
  View,
  ViewStyle,
} from "react-native"
import { CarouselScreenProps } from "./carousel-screen.props"
import { Header } from "../header/header"
import { useSafeArea } from "react-native-safe-area-context"
import { offsets, presets } from "../screen/screen.presets"
import { color, spacing } from "../../theme"
import { useRef } from "react"

const isIos = Platform.OS === "ios"

const CARD: ViewStyle = {
  borderTopLeftRadius: 35,
  borderTopRightRadius: 35,
  backgroundColor: color.palette.white,
  paddingHorizontal: spacing[5],
  paddingTop: spacing[5],
  paddingBottom: spacing[8],
  shadowColor: color.palette.black,
  shadowOffset: { width: 0, height: -10 },
  shadowOpacity: 0.15,
  shadowRadius: 5,
  zIndex: 9,
  marginTop: -35
}

const HEADER: ViewStyle = {
  position: "absolute",
  zIndex: 10,
  top: 10,
}

const CAROUSEL: ViewStyle = {
  // position: "absolute",
  aspectRatio: 1,
  zIndex: 8,
  transform: [{ translateY: 0 }],
}

/**
 * Describe your component here
 */
export function CarouselScreen(props: CarouselScreenProps) {
  const preset = presets.scroll

  const carouselTranslateY = useRef(new Animated.Value(0)).current
  const { top: SafeAreaTop } = useSafeArea()

  const scrollViewStyle = {
    ...preset.outer,
  }
  const cardStyle = { ...CARD }
  const carouselStyle = { ...CAROUSEL }

  return (
    <KeyboardAvoidingView
      style={[preset.outer, {backgroundColor: color.palette.white}]}
      behavior={isIos ? "padding" : null}
      keyboardVerticalOffset={offsets[props.keyboardOffset || "none"]}
    >
      <Header theme="transparent" style={{ ...HEADER, top: SafeAreaTop }} />
      <Animated.ScrollView
        style={scrollViewStyle}
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: {
                  y: carouselTranslateY,
                },
              },
            },
          ],
          { useNativeDriver: true },
        )}
        scrollEventThrottle={1}
        refreshControl={props.refreshControl}
      >
        <Animated.View style={[carouselStyle, { transform: [{ translateY: carouselTranslateY }] }]}>
          {props.carousel}
        </Animated.View>
        <View style={cardStyle}>{props.children}</View>
      </Animated.ScrollView>
    </KeyboardAvoidingView>
  )
}
