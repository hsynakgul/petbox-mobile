import { RefreshControlProps, ViewStyle } from "react-native"
import { KeyboardOffsets } from "../screen/screen.presets"
import * as React from "react"

export interface CarouselScreenProps {
  /**
   * Children components.
   */
  children?: React.ReactNode

  /**
   * Carousel components.
   */
  carousel?: React.ReactNode

  /**
   * Carousel components.
   */
  refreshControl?: React.ReactElement<RefreshControlProps>

  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle

  /**
   * By how much should we offset the keyboard? Defaults to none.
   */
  keyboardOffset?: KeyboardOffsets
}
