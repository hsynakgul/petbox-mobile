import { TextInputProps, TextStyle, ViewStyle } from "react-native"
import { TextInputMaskOptionProp, TextInputMaskTypeProp } from "react-native-masked-text"
import { SelectableList } from "../selectable-list/selectable-list"
import React from "react"

export interface TextFieldLabelProps {
  preset?: "default" | "horizontal"
}

export interface TextFieldProps extends TextInputProps {
  /**
   * The placeholder i18n key.
   */
  placeholderTx?: string

  /**
   * The Placeholder text if no placeholderTx is provided.
   */
  placeholder?: string

  /**
   * The label i18n key.
   */
  labelTx?: string

  /**
   * The label text if no labelTx is provided.
   */
  label?: string

  prependIcon?: string

  appendIcon?: string

  /**
   * Optional container style overrides useful for margins & padding.
   */
  style?: ViewStyle | ViewStyle[]

  /**
   * Optional container style overrides useful for margins & padding.
   */
  contentStyle?: ViewStyle | ViewStyle[]

  /**
   * Optional style overrides for the input.
   */
  inputStyle?: TextStyle | TextStyle[]

  /**
   * Various look & feels.
   */
  preset?: "default" | "horizontal"

  forwardedRef?: any

  errors?: string[]

  hideErrorDetail?: boolean

  mask?: {
    type: TextInputMaskTypeProp
    options?: TextInputMaskOptionProp
    checkText?: (previous: string, next: string) => boolean
    onChangeText?: (text: string, rawText?: string) => void
    refInput?: (ref: any) => void
    customTextInput?: any
    customTextInputProps?: Object
    includeRawValueInChangeText?: boolean
  } | null
}
