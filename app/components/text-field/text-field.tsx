import React from "react"
import { View, TextInput, TextStyle, ViewStyle } from "react-native"
import { color, spacing } from "../../theme"
import { translate } from "../../i18n"
import { TextFieldProps } from "./text-field.props"
import { Text } from ".."
import { Icon } from "../../utils/icons"
import { TextInputMask } from "react-native-masked-text"
import {
  CONTAINER_PRESETS,
  CONTENT_PRESETS,
  INPUT_PRESETS,
  LABEL_PRESETS,
} from "./text-field.presets"

// the base styling for the container

const ICON: ViewStyle = {
  backgroundColor: color.palette.inputAddonBg,
  alignItems: "center",
  justifyContent: "center",
  padding: spacing[4],
}

const ERROR_CONTAINER: ViewStyle = {
  paddingTop: spacing[1],
  paddingHorizontal: spacing[4],
}

const ERROR_ITEM: ViewStyle = {
  marginBottom: spacing[1],
  flexDirection: "row",
}

const ERROR_BADGE: ViewStyle = {
  width: 5,
  height: 5,
  borderRadius: 3,
  borderWidth: 1,
  borderColor: color.error,
  marginTop: 5,
  marginRight: 5,
}

const ERROR_TEXT: TextStyle = {
  fontSize: 12,
  color: color.error,
}

/**
 * A component which has a label and an input together.
 */
export function TextField(props: TextFieldProps) {
  const {
    placeholderTx,
    placeholder,
    labelTx,
    label,
    preset = "default",
    style: styleOverride,
    inputStyle: inputStyleOverride,
    forwardedRef,
    prependIcon = null,
    appendIcon = null,
    errors = [],
    hideErrorDetail = false,
    mask = null,
    multiline = false,
    ...rest
  } = props

  const actualPlaceholder = placeholderTx ? translate(placeholderTx) : placeholder

  let containerStyle: ViewStyle = { ...CONTAINER_PRESETS[preset], ...styleOverride }
  let inputContentStyle: TextStyle = { ...CONTENT_PRESETS[preset] }
  let inputStyle: TextStyle = { ...INPUT_PRESETS[preset], ...inputStyleOverride }

  if (multiline) {
    containerStyle = { ...containerStyle, ...CONTAINER_PRESETS.multiline }
    inputContentStyle = { ...inputContentStyle, ...CONTENT_PRESETS.multiline }
    inputStyle = { ...inputStyle, ...INPUT_PRESETS.multiline }
  }

  const hasLabel = !!(labelTx || label)

  if (hasLabel && preset !== "horizontal") {
    containerStyle = {
      ...containerStyle,
      height: 70,
      paddingTop: spacing[1],
      paddingHorizontal: spacing[2],
    }
  }

  // region renderFunctions
  // endregion

  return (
    <View>
      <View style={containerStyle}>
        <TextFieldPrependIcon icon={prependIcon} hasError={errors.length > 0} />
        <View style={inputContentStyle}>
          <TextFieldLabel text={label} tx={labelTx} hasError={errors.length > 0} preset={preset} />
          {mask ? (
            <TextInputMask
              placeholder={actualPlaceholder}
              placeholderTextColor={color.palette.lighterGrey}
              underlineColorAndroid={color.transparent}
              {...rest}
              style={inputStyle}
              ref={forwardedRef}
              {...mask}
            />
          ) : (
            <TextInput
              placeholder={actualPlaceholder}
              placeholderTextColor={color.palette.lighterGrey}
              underlineColorAndroid={color.transparent}
              multiline={multiline}
              {...rest}
              style={inputStyle}
              ref={forwardedRef}
            />
          )}
        </View>
        <TextFieldAppendIcon icon={appendIcon} hasError={errors.length > 0} />
      </View>
      <TextFieldErrors errors={errors} hideErrorDetail={hideErrorDetail} />
    </View>
  )
}

export interface TextFieldPrependIconProps {
  icon?: string
  hasError: boolean
}
export function TextFieldPrependIcon(props: TextFieldPrependIconProps) {
  const { icon, hasError } = props
  if (!icon) return null
  return (
    <View style={ICON}>
      <Icon name={icon} size={28} color={hasError ? color.error : color.text} />
    </View>
  )
}

export interface TextFieldAppendIconProps {
  icon?: string
  hasError?: boolean
}
export function TextFieldAppendIcon(props: TextFieldAppendIconProps) {
  const { icon, hasError = false } = props
  if (!icon) return null
  return (
    <View style={ICON}>
      <Icon name={icon} size={28} color={hasError ? color.error : color.text} />
    </View>
  )
}

export interface TextFieldLabelProps {
  text?: string
  tx?: string
  hasError?: boolean
  preset?: "default" | "horizontal"
}
export function TextFieldLabel(props: TextFieldLabelProps) {
  const { text = "", tx = "", hasError = false } = props
  if (!text && !tx) return null
  const style = [LABEL_PRESETS[props.preset]]
  if (hasError) style.push({ color: color.error })
  return <Text preset="fieldLabel" style={style} tx={tx} text={text} transform="uppercase" />
}

export interface TextFieldErrorsProps {
  errors?: string[]
  hideErrorDetail?: boolean
}
export function TextFieldErrors(props: TextFieldErrorsProps) {
  const { errors = [], hideErrorDetail= false } = props
  if (errors.length === 0 || hideErrorDetail) return null
  return (
    <View style={ERROR_CONTAINER}>
      {errors.map((error, index) => (
        <View style={ERROR_ITEM} key={`error-${index}`}>
          <View style={ERROR_BADGE} />
          <Text style={ERROR_TEXT} text={error} />
        </View>
      ))}
    </View>
  )
}
