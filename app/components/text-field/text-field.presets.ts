import { TextStyle, ViewStyle } from "react-native"
import { sizes } from "../../theme/sizes"
import { color, spacing, typography } from "../../theme"
import { widthPercentageToDP } from "react-native-responsive-screen"

export const LABEL_PRESETS: {
  [name: string]: TextStyle
} = {
  default: {
    fontSize: sizes.small,
    color: color.dim,
    marginBottom: spacing[1],
  },
  horizontal: {
    fontSize: sizes.medium,
    color: color.text,
    marginRight: spacing[1],
    paddingLeft: spacing[5],
    width: widthPercentageToDP("35%"),
  },
}

const HORIZONTAL_CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
  overflow: "hidden",
  flexDirection: "row",
  justifyContent: "space-between",
  height: 50,
  borderBottomWidth: 1,
  borderBottomColor: color.borderLight,
}

export const CONTAINER_PRESETS: {
  [name: string]: ViewStyle
} = {
  default: {
    backgroundColor: color.palette.inputBg,
    borderRadius: 18,
    overflow: "hidden",
    flexDirection: "row",
    justifyContent: "space-between",
    height: 70,
  },
  horizontal: {
    ...HORIZONTAL_CONTAINER,
  },
  multiline: {
    ...HORIZONTAL_CONTAINER,
    height: "auto",
  },
}

export const CONTENT_PRESETS: {
  [name: string]: ViewStyle
} = {
  default: {
    flex: 1,
    justifyContent: "center",
    paddingVertical: spacing[3],
    paddingHorizontal: spacing[4],
  },
  horizontal: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  multiline: {
    flexDirection: "column",
    alignItems: "flex-start",
    paddingTop: spacing[4],
  },
}

const HORIZONTAL_INPUT: TextStyle = {
  flex: 1,
  fontFamily: typography.primary,
  color: color.text,
  minHeight: 28,
  fontSize: sizes.header,
  textAlignVertical: "bottom",
  paddingTop: 0,
  paddingLeft: spacing[3],
  paddingRight: spacing[5],
}
export const INPUT_PRESETS: {
  [name: string]: TextStyle
} = {
  default: {
    fontFamily: typography.primary,
    color: color.text,
    minHeight: 28,
    fontSize: sizes.mediumPlus,
    textAlignVertical: "bottom",
    padding: 0,
  },
  horizontal: {
    ...HORIZONTAL_INPUT
  },
  multiline: {
    ...HORIZONTAL_INPUT,
    paddingLeft: spacing[5],
    marginTop: spacing[3],
    paddingBottom: spacing[3]
  }
}
