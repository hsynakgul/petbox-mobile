import * as React from "react"
import { Image, ImageStyle, TextStyle, View, ViewStyle } from "react-native"
import { color, spacing } from "../../theme"
import { Text } from "../"
import { sizes } from "../../theme/sizes"

const BORED_IMAGE = require("../../../assets/image/bored.png")

const CONTAINER: ViewStyle = {
  flex: 1,
  alignItems: "center",
  justifyContent: "center",
}

const IMAGE: ImageStyle = {
  backgroundColor: color.background,
  width: 250,
  height: 150,
  resizeMode: "contain",
}

const TEXT: TextStyle = {
  marginTop: spacing[4],
  fontSize: sizes.mediumPlus,
}

export interface NotFoundProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle

  text?: string
}

/**
 * Describe your component here
 */
export function NotFound(props: NotFoundProps) {
  const { style } = props

  return (
    <View style={[CONTAINER, style]}>
      <Image source={BORED_IMAGE} style={IMAGE} />
      <Text text={props.text} style={TEXT} />
    </View>
  )
}
