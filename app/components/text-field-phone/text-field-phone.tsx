import * as React from "react"
import { TextField } from "../"
import { TextInputMaskProps } from "react-native-masked-text"
import { TextFieldProps } from "../text-field/text-field.props"

export interface TextFieldPhoneProps extends TextFieldProps {}

/**
 * Describe your component here
 */
export function TextFieldPhone(props: TextFieldPhoneProps) {
  const { style } = props
  const mask = {
    type: "custom",
    options: { mask: "599 999 99 99" },
  } as TextInputMaskProps
  return (
    <TextField
      placeholder="5XX XXX XX XX"
      keyboardType="phone-pad"
      autoCapitalize="none"
      autoCorrect={false}
      mask={mask}
      {...props}
      style={style}
    />
  )
}
