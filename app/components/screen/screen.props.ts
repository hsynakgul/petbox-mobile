import { Animated, ColorValue, ViewStyle } from "react-native"
import { KeyboardOffsets, ScreenPresets } from "./screen.presets"
import { HeaderProps } from "../header/header.props"

export interface ScreenProps {
  /**
   * Children components.
   */
  children?: React.ReactNode

  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle

  /**
   * One of the different types of presets.
   */
  preset?: ScreenPresets

  /**
   * An optional background color
   */
  backgroundColor?: string

  /**
   * An optional status bar setting. Defaults to light-content.
   */
  statusBar?: "light-content" | "dark-content"

  statusBarBg?: ColorValue

  /**
   * Should we not wrap in SafeAreaView? Defaults to false.
   */
  unsafe?: boolean

  /**
   * By how much should we offset the keyboard? Defaults to none.
   */
  keyboardOffset?: KeyboardOffsets

  header?: HeaderProps

  rounded?: boolean

  visibleStatusBar?: boolean

  handleScroll?(event: object): void

  corner?: Animated.Value

  defaultCorner?: number

  paddingTop?: number

  behavior?: "padding" | null
}
