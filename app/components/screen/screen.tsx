import * as React from "react"
import { Animated, Easing, KeyboardAvoidingView, Platform, View } from "react-native"
import { ScreenProps } from "./screen.props"
import { isNonScrolling, offsets, presets } from "./screen.presets"
import { Header, StatusBar } from ".."
import { flatten, mergeAll, pathOr } from "ramda"
import { color, spacing } from "../../theme"
import { forwardRef, RefObject, useImperativeHandle, useState } from "react"

const isIos = Platform.OS === "ios"

export interface ScreenRefProps {
  handleScroll(event: object): void
}

function ScreenWithoutScrolling(props: ScreenProps) {
  const { visibleStatusBar = true, corner = 0, behavior = "padding" } = props
  const preset = presets.fixed
  const header = props.header || {}
  const backgroundStyle = { backgroundColor: props.backgroundColor || color.background }

  const style = [preset.outer, backgroundStyle]
  if (props.rounded === true) style.push({ paddingTop: spacing[0] })
  const viewStyle = mergeAll(flatten(style))

  return (
    <KeyboardAvoidingView
      style={[preset.outer]}
      behavior={isIos ? behavior : null}
      keyboardVerticalOffset={offsets[props.keyboardOffset || "none"]}
    >
      {visibleStatusBar ? (
        <StatusBar
          barStyle={props.statusBar || "light-content"}
          backgroundColor={props.statusBarBg || null}
        />
      ) : null}
      <Header {...header} />
      <Animated.View
        style={[viewStyle, { borderTopRightRadius: corner, borderTopLeftRadius: corner }]}
      >
        {props.children}
      </Animated.View>
    </KeyboardAvoidingView>
  )
}

function ScreenWithScrolling(props: ScreenProps) {
  const { visibleStatusBar = true, handleScroll, corner = 0, paddingTop = null, behavior = "padding" } = props
  const preset = presets.scroll
  const header = props.header || {}
  const backgroundStyle = { backgroundColor: props.backgroundColor || color.background }

  const style = [preset.outer, backgroundStyle]
  if (props.rounded === true)
    style.push({ paddingTop: paddingTop !== null ? paddingTop : spacing[6] })
  const viewStyle = mergeAll(flatten(style))
  return (
    <KeyboardAvoidingView
      style={[preset.outer]}
      behavior={isIos ? behavior : null}
      keyboardVerticalOffset={offsets[props.keyboardOffset || "none"]}
    >
      {visibleStatusBar ? (
        <StatusBar
          barStyle={props.statusBar || "light-content"}
          backgroundColor={props.statusBarBg || null}
        />
      ) : null}

      <Header {...header} />
      <View style={preset.outer}>
        <Animated.ScrollView
          style={[viewStyle, { borderTopRightRadius: corner, borderTopLeftRadius: corner }]}
          keyboardShouldPersistTaps="handled"
          onScroll={handleScroll}
          scrollEventThrottle={10}
        >
          {props.children}
        </Animated.ScrollView>
      </View>
    </KeyboardAvoidingView>
  )
}

/**
 * The starting component on every screen in the app.
 *
 * @param props The screen props
 */
export const Screen = forwardRef((props: ScreenProps, ref: RefObject<ScreenRefProps>) => {
  const { defaultCorner = 35 } = props
  const [corner] = useState(new Animated.Value(props.rounded === true ? defaultCorner : 0))
  const handleScroll = (event: Object) => {
    const offsetY = pathOr(0, ["nativeEvent", "contentOffset", "y"], event)
    if (props.rounded === true) {
      let toValue = offsetY <= 0 ? 0 : offsetY >= defaultCorner ? defaultCorner : offsetY
      Animated.timing(corner, {
        duration: 0,
        toValue: defaultCorner - toValue,
        easing: Easing.linear,
        useNativeDriver: true,
        delay: 0,
      }).start()
    }
  }
  useImperativeHandle(ref, () => ({
    handleScroll: handleScroll,
  }))
  if (isNonScrolling(props.preset)) {
    return <ScreenWithoutScrolling {...props} corner={corner} />
  } else {
    return <ScreenWithScrolling {...props} corner={corner} handleScroll={handleScroll} />
  }
})
