import * as React from "react"
import {
  Dimensions,
  FlatList,
  Image,
  ImageStyle,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { color, spacing } from "../../theme"
import { Spinner, Text } from "../"
import { Icon } from "../../utils/icons"
import ImagePicker from "react-native-image-crop-picker"
import { useCallback, useEffect, useState } from "react"
import { Api, File } from "../../services/api"
import { findIndex, propEq } from "ramda"

const CONTAINER: ViewStyle = {
  padding: spacing[3],
}

const BUTTON: TextStyle = {
  flexDirection: "row",
  alignItems: "center",
  paddingHorizontal: spacing[3],
}

const IMAGE_CONTAINER: ViewStyle = {
  padding: spacing[1],
  borderStyle: "dashed",
  borderWidth: 1,
  borderColor: color.palette.lightGrey,
  borderRadius: spacing[3],
  height: 150,
  width: Dimensions.get("window").width / 3 - spacing[1] * 4,
  marginHorizontal: spacing[1],
  marginVertical: spacing[2],
  position: "relative",
}

const IMAGE: ImageStyle = {
  width: "100%",
  height: "100%",
  borderRadius: spacing[2],
  position: "relative",
  zIndex: 1,
}

const IMAGE_LOADING: ImageStyle = {
  position: "absolute",
  left: spacing[1],
  top: spacing[1],
  width: "100%",
  height: "100%",
  borderRadius: spacing[2],
  backgroundColor: color.blackHalfTransparent,
  zIndex: 2,
  alignItems: "center",
  justifyContent: "center",
}

const DELETE_PHOTO: TextStyle = {
  position: "absolute",
  right: -5,
  top: -10,
  backgroundColor: color.error,
  padding: spacing[2],
  zIndex: 3,
  borderRadius: 20,
}

export interface SelectPhotosProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle

  errors?: string[]

  photos?: File[]

  setPhotos?(val: File[]): void

  uploading?: boolean

  setUploading?(val: boolean): void
}

export interface SelectFile extends File {
  uploading?: {
    loading?: boolean
    mime?: string
    uri?: string
    filename?: string
    width?: number
    height?: number
    size?: number
  }
}

/**
 * Describe your component here
 */
export function SelectPhotos(props: SelectPhotosProps) {
  const { style, errors = [], photos = [], setPhotos, setUploading } = props

  const [list, setList] = useState<SelectFile[]>(photos)
  const [waitingUpload, setWaitingUpload] = useState<string[]>([])
  const [api] = useState<Api>(new Api())

  useEffect(() => {
    api.setup()
  }, [])

  useEffect(() => {
    setList(photos)
  }, [photos])

  useEffect(() => {
    if (waitingUpload.length > 0) {
      const id = waitingUpload[0]
      const index = findIndex(propEq("id", id), list)
      if (index > -1) {
        const uploadFile: SelectFile = list[index]
        api
          .storeFile({
            filename: uploadFile.uploading.filename,
            uri: uploadFile.uploading.uri,
            type: uploadFile.uploading.mime,
            width: uploadFile.uploading.width,
            height: uploadFile.uploading.height,
            size: uploadFile.uploading.size,
          })
          .then((rsp) => {
            const arr: SelectFile[] = [...list.filter((item) => item.id !== id)]
            if (rsp.kind === "ok") {
              arr.push({
                ...rsp.data,
                uploading: {
                  ...uploadFile.uploading,
                  loading: false,
                },
              })
            }
            setList(arr)
            setWaitingUpload(waitingUpload.slice(1))
          })
      } else {
        setWaitingUpload(waitingUpload.slice(1))
      }
    } else {
      setUploading(false)
      setPhotos(
        list.map((item) => {
          const { uploading, ...ctx } = item
          return ctx
        }),
      )
    }
  }, [waitingUpload])

  const openPicker = useCallback(() => {
    ImagePicker.openPicker({
      multiple: true,
      maxFiles: 6,
      mediaType: "photo",
      cropping: true,
    }).then((result) => {
      const ids: string[] = []
      setList(
        result.map((item) => {
          ids.push(item.localIdentifier)
          return {
            id: item.localIdentifier,
            uploading: {
              uri: item.sourceURL,
              filename: item.filename,
              type: item.mime,
              width: item.width,
              height: item.height,
              size: item.size,
              loading: true,
            },
          }
        }) as SelectFile[],
      )
      if (typeof setUploading === "function") setUploading(true)
      setWaitingUpload(ids)
    })
  }, [])

  const deletePhoto = (id: any) => {
    const arr = list.filter((item) => item.id !== id)
    setList(arr)
    setPhotos(
      arr.map((item) => {
        const { uploading, ...ctx } = item
        return ctx
      }),
    )
  }

  const renderDeletePhoto = (item: SelectFile) => {
    if (item?.uploading?.loading) return null
    return (
      <TouchableOpacity style={DELETE_PHOTO} onPress={() => deletePhoto(item.id)}>
        <Icon name="close-square" size={20} color={color.palette.white} />
      </TouchableOpacity>
    )
  }

  const renderLoading = (item: SelectFile) => {
    if (!item?.uploading?.loading) return null
    return (
      <View style={[IMAGE_LOADING]}>
        <Spinner size={26} color={color.primary} />
      </View>
    )
  }

  const renderItem = ({ item }: { item: SelectFile }) => {
    return (
      <View style={IMAGE_CONTAINER}>
        {renderDeletePhoto(item)}
        <Image source={{ uri: item?.uploading?.uri || item.thumb }} style={IMAGE} />
        {renderLoading(item)}
      </View>
    )
  }
  return (
    <View style={[CONTAINER, style]}>
      <View style={{ alignItems: "flex-end", marginBottom: spacing[2] }}>
        <TouchableOpacity style={BUTTON} onPress={openPicker}>
          <Icon name="image" size={20} color={errors.length > 0 ? color.error : color.primary} />
          <Text
            style={{
              marginLeft: spacing[2],
              color: errors.length > 0 ? color.error : color.primary,
            }}
          >
            Fotoğraf Seç
          </Text>
        </TouchableOpacity>
      </View>
      {list.length > 0 ? (
        <FlatList
          data={list}
          renderItem={renderItem}
          numColumns={3}
          keyExtractor={(item) => item.id as string}
          contentContainerStyle={{ paddingTop: spacing[4] }}
          horizontal={false}
        />
      ) : null}
    </View>
  )
}
