import { TextStyle, ViewStyle } from "react-native"
import { color, spacing, typography } from "../../theme"
import { heightPercentageToDP, widthPercentageToDP } from "react-native-responsive-screen"

const BASE: ViewStyle = {
  width: "100%",
  height: "100%",
  backgroundColor: color.palette.white,
  shadowColor: color.dim,
  shadowOffset: { width: 0, height: 0 },
  shadowRadius: 4,
  shadowOpacity: 0.1,
  overflow: "hidden"
}

const CENTER: ViewStyle = {
  width: "auto",
  height: "auto",
  minWidth: widthPercentageToDP("75%"),
  maxWidth: widthPercentageToDP("95%"),
  minHeight: 100,
  maxHeight: heightPercentageToDP("95%"),
  backgroundColor: color.palette.white,
  borderRadius: spacing[5]
}

const TOP: ViewStyle = {
  width: "100%",
  height: "auto",
  minHeight: 100,
  maxHeight: heightPercentageToDP("90%"),
  backgroundColor: color.palette.white,
  borderBottomLeftRadius: spacing[5],
  borderBottomRightRadius: spacing[5],
  position: "absolute",
  top: 0,
  shadowOffset: { width: 0, height: 10 },
}

const BOTTOM: ViewStyle = {
  width: "100%",
  height: "auto",
  minHeight: 100,
  maxHeight: heightPercentageToDP("90%"),
  backgroundColor: color.palette.white,
  borderTopLeftRadius: spacing[5],
  borderTopRightRadius: spacing[5],
  position: "absolute",
  bottom: 0,
  shadowOffset: { width: 0, height: -10 },
}

/**
 * All the variations of text styling within the app.
 *
 * You want to customize these to whatever you need in your app.
 */
export const presets = {
  /**
   * The default text styles.
   */
  default: BASE,

  center: CENTER,

  top: TOP,

  bottom: BOTTOM,

}

/**
 * A list of preset names.
 */
export type TextPresets = keyof typeof presets
