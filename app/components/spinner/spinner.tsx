import * as React from "react"
import { Animated, Easing, ViewStyle } from "react-native"
import { color as colorSelection } from "../../theme"
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5"
import { useMemo, useState } from "react"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

export interface SpinnerProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  size?: number
  color?: string
}

/**
 * Describe your component here
 */
export function Spinner(props: SpinnerProps) {
  const { style, size = 24, color = colorSelection.text } = props

  const [spinValue] = useState<Animated.Value>(new Animated.Value(0))

  const startAnimation = useMemo(
    () => () => {
      Animated.loop(
        Animated.timing(spinValue, {
          toValue: 1,
          duration: 1500,
          easing: Easing.linear,
          useNativeDriver: true,
        }),
      ).start()
    },
    [spinValue],
  )

  startAnimation()
  const rotate = spinValue.interpolate({ inputRange: [0, 1], outputRange: ["0deg", "360deg"] })
  return (
    <Animated.View style={[CONTAINER, style, { transform: [{ rotate }] }]}>
      <FontAwesome5Icon name="spinner" size={size} color={color} />
    </Animated.View>
  )
}
