import * as React from "react"
import {
  View,
  ViewStyle,
  Modal as RNModal,
  SafeAreaView,
  TouchableOpacity,
  TouchableWithoutFeedback, ModalBaseProps,
} from "react-native"
import { color } from "../../theme"
import { presets } from "./bottom-sheet-modal.presets"
import { forwardRef, ReactComponentElement, RefObject, useImperativeHandle, useState } from "react"
import { flatten, mergeAll } from "ramda"

const CONTAINER: ViewStyle = {
  backgroundColor: color.modal,
  flex: 1,
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "center",
}

export interface ModalProps extends ModalBaseProps{
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  children?: ReactComponentElement<any>
}

export interface ModalRefProps {
  open(props: ModalOpenProps): void
  close(): void
}

export interface ModalOpenProps {
  display?: "default" | "top" | "bottom" | "center"
}

/**
 * Describe your component here
 */
export const BottomSheetModal = forwardRef((props: ModalProps, ref: RefObject<ModalRefProps>) => {
  const [containerStyle, setContainerStyle] = useState<ViewStyle>(null)

  const [visible, setVisible] = useState<boolean>(false)
  const close = () => {
    setVisible(false)
  }

  useImperativeHandle(ref, () => ({
    open(props: ModalOpenProps = {}) {
      const { display = "default" } = props
      setContainerStyle(mergeAll(flatten([presets.default, presets[display]])))
      setVisible(true)
    },
    close: close,
  }))

  return (
    <RNModal animated animationType="fade" transparent={true} visible={visible} {...props}>
      <TouchableOpacity style={CONTAINER} onPress={close}>
        <TouchableWithoutFeedback>
          <View style={containerStyle}>
            <SafeAreaView>{props.children}</SafeAreaView>
          </View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </RNModal>
  )
})
