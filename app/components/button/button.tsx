import * as React from "react"
import { TouchableOpacity } from "react-native"
import { viewPresets, textPresets } from "./button.presets"
import { ButtonProps } from "./button.props"
import { mergeAll, flatten } from "ramda"
import { Icon } from "../../utils/icons"
import { Spinner, Text } from ".."

/**
 * For your text displaying needs.
 *
 * This component is a HOC over the built-in React Native one.
 */
export function Button(props: ButtonProps) {
  // grab the props
  const {
    preset = "primary",
    tx,
    text,
    rounded = true,
    style: styleOverride,
    textStyle: textStyleOverride,
    children,
    textTransform = null,
    loading = false,
    loadingTx = "",
    loadingText = "",
    prependIcon = "",
    appendIcon = "",
    ...rest
  } = props

  const styles = [viewPresets[preset] || viewPresets.primary, styleOverride]
  const viewStyle = mergeAll(flatten(styles))
  const textStyle = mergeAll(
    flatten([textPresets[preset] || textPresets.primary, textStyleOverride]),
  )

  let content = children || <Text tx={tx} text={text} style={textStyle} transform={textTransform} />
  if (loading)
    content =
      loadingTx || loadingText ? (
        <Text tx={loadingTx} text={loadingText} style={textStyle} transform={textTransform} />
      ) : null

  // region renderFunctions
  const renderPrependIcon = () => {
    if (loading) {
      return <Spinner size={22} color={textStyle.color} />
    }
    if (!prependIcon) return null
    return <Icon name={prependIcon} size={20} color={textStyle.color} />
  }

  // region renderFunctions
  const renderAppendIcon = () => {
    if (!appendIcon || loading) return null
    return <Icon name={appendIcon} size={20} color={textStyle.color} />
  }

  return (
    <TouchableOpacity style={viewStyle} {...rest}>
      {renderPrependIcon()}
      {content}
      {renderAppendIcon()}
    </TouchableOpacity>
  )
}
