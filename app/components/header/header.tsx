import React, { useCallback, useState } from "react"
import { View, ViewStyle, TextStyle } from "react-native"
import { HeaderProps } from "./header.props"
import { Button, Text } from ".."
import { spacing } from "../../theme"
import { translate } from "../../i18n/"
import { useFocusEffect, useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"
import { Icon } from "../../utils/icons"
import { presets } from "./header.presets"
import { sizes } from "../../theme/sizes"

// static styles
const COVER: ViewStyle = {
  height: 110,
  position: "absolute",
  top: 0,
  width: "100%",
}
const ROOT: ViewStyle = {
  position: "relative",
  flexDirection: "row",
  alignItems: "center",
  paddingTop: spacing[2],
  paddingBottom: spacing[2],
  justifyContent: "flex-start",
  height: 60,
}
const TITLE: TextStyle = { textAlign: "left", fontSize: sizes.header }
const TITLE_MIDDLE: ViewStyle = { flex: 1, justifyContent: "center", paddingHorizontal: spacing[0] }
const LEFT_BUTTONS: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  marginRight: spacing[3],
}
const BUTTON: ViewStyle = { height: 32, flexDirection: "row", alignItems: "center" }

/**
 * Header that appears on many screens. Will hold navigation buttons and screen title.
 */
export const Header = observer(function (props: HeaderProps) {
  const {
    onLeftPress,
    onRightPress,
    rightIcon,
    rightText,
    leftIcon,
    headerText,
    headerTx,
    style,
    titleStyle,
    theme = "default",
  } = props
  const header = headerText || (headerTx && translate(headerTx)) || ""

  const [showBackButton, setShowBackButton] = useState<boolean>(false)

  const navigation = useNavigation()
  useFocusEffect(
    useCallback(() => {
      setShowBackButton(navigation.canGoBack())
    }, []),
  )

  // region render function
  const leftButtons = () => {
    if (!leftIcon && !showBackButton) return null
    return (
      <View style={LEFT_BUTTONS}>
        {showBackButton ? (
          <Button preset="link" style={BUTTON} onPress={() => navigation.goBack()}>
            <Icon name="arrow-left" size={sizes.headerIcon} style={presets.text[theme]} />
          </Button>
        ) : null}
        {leftIcon ? (
          <Button preset="link" onPress={onLeftPress} style={BUTTON}>
            <Icon name={leftIcon} size={sizes.headerIcon} style={presets.text[theme]} />
          </Button>
        ) : null}
      </View>
    )
  }
  const rightButtons = () => {
    if (!rightIcon && !rightText) return null
    return (
      <Button preset="link" onPress={onRightPress} style={BUTTON}>
        {rightText ? (
          <Text tx={rightText} text={rightText} style={{ ...presets.text[theme], marginRight: spacing[2] }} />
        ) : null}
        {rightIcon ? (
          <Icon name={rightIcon} size={sizes.headerIcon} style={presets.text[theme]} />
        ) : null}
      </Button>
    )
  }
  if (!leftIcon && !showBackButton && !rightIcon && !header) return null
  //endregion
  return (
    <View style={{ ...ROOT, ...style }}>
      <View style={{ ...presets[theme], ...COVER }} />
      <View style={{ paddingHorizontal: spacing[5], flexDirection: "row", alignItems: "center" }}>
        {leftButtons()}
        <View style={TITLE_MIDDLE}>
          <Text
            style={{ ...TITLE, ...titleStyle, ...presets.text[theme] }}
            text={header}
            numberOfLines={1}
            ellipsizeMode="tail"
            transform="capitalize"
          />
        </View>
        {rightButtons()}
      </View>
    </View>
  )
})
