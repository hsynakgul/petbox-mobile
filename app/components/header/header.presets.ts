import { TextStyle, ViewStyle } from "react-native"
import { color } from "../../theme"

/**
 * All the variations of screens.
 */
export const presets = {
  default: {
    backgroundColor: color.palette.inputAddonBg
  } as ViewStyle,
  primary: {
    backgroundColor: color.primary
  } as ViewStyle,
  orange: {
    backgroundColor: color.palette.orangeDarker
  } as ViewStyle,
  pink: {
    backgroundColor: color.palette.pink
  } as ViewStyle,
  purple: {
    backgroundColor: color.palette.purple
  } as ViewStyle,
  white: {
    backgroundColor: color.palette.white
  } as ViewStyle,
  transparent: {
    backgroundColor: color.transparent
  } as ViewStyle,
  text: {
    default: {
      color: color.text
    } as TextStyle,
    primary: {
      color: color.palette.white
    } as TextStyle,
    orange: {
      color: color.palette.white
    } as TextStyle,
    pink: {
      color: color.palette.white
    } as TextStyle,
    purple: {
      color: color.palette.white
    } as TextStyle,
    white: {
      color: color.text
    } as TextStyle,
    transparent: {
      color: color.palette.white
    } as TextStyle,
  }
}

/**
 * The variations of screens.
 */
export type HeaderPresets = keyof typeof presets
