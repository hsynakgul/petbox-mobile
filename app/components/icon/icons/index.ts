export const icons = {
  bullet: require("./bullet.png"),
  account: require("./account.png"),
  notification: require("./notification.png"),
  voice: require("./voice.png"),
  logout: require("./logout.png"),
  category_2: require("./dog-icon.png"),
  category_1: require("./cat-icon.png"),
  category_3: require("./bird-icon.png"),
  category_4: require("./rabbit-icon.png"),
  category_5: require("./fish-icon.png"),
  category_6: require("./turtle-icon.png"),
  category_7: require("./hamster-icon.png"),
  category_8: require("./snake-icon.png"),
}

export type IconTypes = keyof typeof icons
