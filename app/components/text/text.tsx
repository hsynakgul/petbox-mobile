import * as React from "react"
import { Text as ReactNativeText } from "react-native"
import { presets } from "./text.presets"
import { TextProps } from "./text.props"
import { translate } from "../../i18n"
import { mergeAll, flatten, is } from "ramda"

/**
 * For your text displaying needs.
 *
 * This component is a HOC over the built-in React Native one.
 */
export function Text(props: TextProps) {
  // grab the props
  const { preset = "default", tx, txOptions, text, children, style: styleOverride, ...rest } = props

  const isString = is(String)
  const isNumber = is(Number)
  // figure out which content to use
  const i18nText = (tx && translate(tx, txOptions)) || tx
  let content = i18nText || text || children
  if (!content) {
    if (i18nText || isNumber(i18nText)) content = i18nText
    else if (text || isNumber(text)) content = text
    else if (children || isNumber(children)) content = children
  }
  if (rest.transform && isString(content)) {
    switch (rest.transform) {
      case "capitalize":
        content = content.toString().toCapitalizeTurkish()
        break
      case "lowercase":
        content = content.toString().toLowerCaseTurkish()
        break
      case "uppercase":
        content = content.toString().toUpperCaseTurkish()
        break
      default:
        break
    }
  }

  const style = mergeAll(flatten([presets[preset] || presets.default, styleOverride]))

  return (
    <ReactNativeText {...rest} style={style}>
      {content}
    </ReactNativeText>
  )
}
