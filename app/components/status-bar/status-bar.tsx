import * as React from "react"
import {
  View,
  ViewStyle,
  StatusBar as ReactNativeStatusBar,
  Platform,
  ColorValue,
} from "react-native"
import { color } from "../../theme"
import { useSafeArea } from "react-native-safe-area-context"

const STATUSBAR_HEIGHT = Platform.OS === "ios" ? 20 : ReactNativeStatusBar.currentHeight

const CONTAINER: ViewStyle = {
  justifyContent: "center",
  backgroundColor: color.background,
  height: STATUSBAR_HEIGHT,
}

export interface StatusBarProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  barStyle?: string
  backgroundColor?: ColorValue
}

/**
 * Describe your component here
 */
export function StatusBar(props: StatusBarProps) {
  const { style, backgroundColor = color.background, ...rest } = props
  const insets = useSafeArea()
  if (insets.top > CONTAINER.height) CONTAINER.height = insets.top
  return (
    <View style={[CONTAINER, style, {backgroundColor: backgroundColor || color.background}]}>
      <ReactNativeStatusBar translucent {...rest} />
    </View>
  )
}
