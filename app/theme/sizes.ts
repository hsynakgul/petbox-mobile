import { Dimensions, Platform, PixelRatio } from "react-native"

const { width: SCREEN_WIDTH } = Dimensions.get("window")

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 320

export function normalize(size) {
  const newSize = size * scale
  if (Platform.OS === "ios") {
    return Math.round(PixelRatio.roundToNearestPixel(newSize))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
  }
}

export const sizes = {
  tiny: normalize(4),
  smaller: normalize(6),
  small: normalize(8),
  medium: normalize(11),
  mediumPlus: normalize(13),
  large: normalize(24),
  huge: normalize(28),
  hugePlus: normalize(36),
  header: normalize(16),
  headerIcon: normalize(20),
}
