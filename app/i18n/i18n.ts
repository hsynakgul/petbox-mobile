import * as RNLocalize from "react-native-localize"
import i18n from "i18n-js"
import RNRestart from "react-native-restart"
import { loadString, remove, saveString } from "../utils/storage"

let useSystemLanguage = false

export const setupLanguage = () => {
  i18n.fallbacks = true

  const fallback = { languageTag: "tr", isRTL: false }
  i18n.translations = { tr: require("./tr.json"), en: require("./en.json") }

  let { languageTag } =
    RNLocalize.findBestAvailableLanguage(Object.keys(i18n.translations)) || fallback

  loadString("languageTag").then((val) => {
    if (val) languageTag = val
    else useSystemLanguage = true
    i18n.locale = languageTag
  })
}

export const setLanguageTag = async (val: string | null) => {
  if (val) {
    await saveString("languageTag", val)
  } else {
    await remove("languageTag")
  }
  RNRestart.Restart()
}

export { i18n, useSystemLanguage }
