import React, { useRef, useState } from "react"
import { observer } from "mobx-react-lite"
import {
  Image,
  ImageStyle,
  TextInput,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { Button, Screen, Text, TextField } from "../../components"
import { useNavigation } from "@react-navigation/native"
import { color, spacing } from "../../theme"
import { useStores } from "../../models"
import { heightPercentageToDP as hp } from "react-native-responsive-screen"
import { pathOr } from "ramda"

const SIGNIN_IMAGE = require("../../../assets/image/sign-in.png")

// const WINDOW_WIDTH = Dimensions.get("window").width

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

const FORM_CONTAINER: ViewStyle = {
  paddingHorizontal: spacing[5],
  paddingBottom: spacing[5],
  paddingTop: spacing[0],
}

const FORGOT_BUTTON: ViewStyle = {
  paddingVertical: spacing[3],
}

const SUBMIT: ViewStyle = {
  marginTop: spacing[6],
}

const IMAGE: ImageStyle = {
  backgroundColor: color.background,
  resizeMode: "contain",
  height: hp("40%"),
}

const DONT_HAVE_ACCOUNT_YET: TextStyle = {
  paddingTop: spacing[6],
  paddingBottom: spacing[3],
  textAlign: "center",
  fontSize: 13,
}

const REGISTER: ViewStyle = {
  alignItems: "center",
}

const REGISTER_TEXT: TextStyle = {
  color: color.palette.blue,
  fontSize: 18,
}

export const SignInScreen = observer(function SignInScreen() {
  const navigation = useNavigation()
  const { authStore } = useStores()

  // region states
  const [processing, setProcessing] = useState<boolean>(false)
  const [email, setEmail] = useState<string>("")
  const [password, setPassword] = useState<string>("")
  const [validationErrors, setValidationErrors] = useState<object>({})
  // endregion

  // region refs
  const refPasswordField = useRef<TextInput>(null)
  // endregion

  // region functions
  const onSubmit = () => {
    if (processing) return null
    setProcessing(true)
    setValidationErrors({})
    authStore.login(email, password).then((rsp) => {
      if (rsp.kind !== "ok") {
        setProcessing(false)
        if (rsp.kind === "validation") {
          setValidationErrors(rsp.validation || {})
        }
      }
    })
  }
  // endregion

  return (
    <Screen style={ROOT} preset="scroll" statusBar="dark-content">
      <Image source={SIGNIN_IMAGE} style={IMAGE} />
      <View style={FORM_CONTAINER}>
        <TextField
          placeholder="example@mail.com"
          keyboardType="email-address"
          autoCapitalize="none"
          autoCorrect={false}
          prependIcon="message"
          returnKeyType="next"
          onSubmitEditing={() => refPasswordField.current.focus()}
          blurOnSubmit={false}
          value={email}
          onChangeText={(val) => setEmail(val)}
          errors={pathOr([], ["email"], validationErrors)}
        />
        <TextField
          style={{ marginTop: spacing[4] }}
          placeholder="******"
          secureTextEntry={true}
          autoCapitalize="none"
          autoCorrect={false}
          prependIcon="lock"
          returnKeyType="done"
          onSubmitEditing={() => onSubmit()}
          blurOnSubmit={false}
          forwardedRef={refPasswordField}
          value={password}
          onChangeText={(val) => setPassword(val)}
          errors={pathOr([], ["password"], validationErrors)}
        />
        <TouchableOpacity style={FORGOT_BUTTON} onPress={() => navigation.navigate("forgot")}>
          <Text tx="signInScreen.forgotPassword" />
        </TouchableOpacity>
        <Button
          preset="blue"
          tx="signInScreen.signIn"
          textTransform="uppercase"
          style={SUBMIT}
          onPress={onSubmit}
          loading={processing}
          appendIcon="arrow-right"
        />
        <Text tx="signInScreen.haveAnAccountYet" style={DONT_HAVE_ACCOUNT_YET} />
        <TouchableOpacity style={REGISTER} onPress={() => navigation.navigate("signUp")}>
          <Text style={REGISTER_TEXT} tx="signInScreen.signUp" transform="uppercase" />
        </TouchableOpacity>
      </View>
    </Screen>
  )
})
