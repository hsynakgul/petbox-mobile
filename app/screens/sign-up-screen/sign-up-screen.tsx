import React, { useRef, useState } from "react"
import { observer } from "mobx-react-lite"
import { Image, ImageStyle, TextInput, View, ViewStyle } from "react-native"
import { Button, Screen, TextField } from "../../components"
import { color, spacing } from "../../theme"
import { heightPercentageToDP as hp } from "react-native-responsive-screen"
import { useStores } from "../../models"
import { pathOr } from "ramda"

const IMAGE_SRC = require("../../../assets/image/sign-up.png")

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

const FORM_CONTAINER: ViewStyle = {
  paddingHorizontal: spacing[5],
  paddingBottom: spacing[5],
  paddingTop: spacing[0],
}

const SUBMIT: ViewStyle = {
  marginTop: spacing[6],
}

const IMAGE_CONTAINER: ViewStyle = {
  alignItems: "center",
}

const IMAGE: ImageStyle = {
  backgroundColor: color.background,
  resizeMode: "contain",
  height: hp("30%"),
}

export const SignUpScreen = observer(function SignUpScreen() {
  const { authStore } = useStores()

  // region states
  const [processing, setProcessing] = useState<boolean>(false)
  const [name, setName] = useState<string>("")
  const [email, setEmail] = useState<string>("")
  const [password, setPassword] = useState<string>("")
  const [validationErrors, setValidationErrors] = useState<object>({})
  // endregion

  // region refs
  const refNameField = useRef<TextInput>(null)
  const refPasswordField = useRef<TextInput>(null)
  // endregion

  // region functions
  const onSubmit = () => {
    if (processing) return null
    setProcessing(true)
    setValidationErrors({})
    authStore.register({ name, email, password }).then((rsp) => {
      if (rsp.kind !== "ok") {
        setProcessing(false)
        if (rsp.kind === "validation") {
          setValidationErrors(rsp.validation || {})
        }
      }
    })
  }
  // endregion

  return (
    <Screen
      style={ROOT}
      preset="scroll"
      statusBar="dark-content"
      header={{ headerTx: "signInScreen.signUp", theme: "white" }}
    >
      <View style={IMAGE_CONTAINER}>
        <Image source={IMAGE_SRC} style={IMAGE} />
      </View>
      <View style={FORM_CONTAINER}>
        <TextField
          placeholder="example@mail.com"
          labelTx="editProfileScreen.email"
          keyboardType="email-address"
          autoCapitalize="none"
          autoCorrect={false}
          returnKeyType="next"
          onSubmitEditing={() => refNameField.current.focus()}
          blurOnSubmit={false}
          style={{ marginBottom: spacing[4] }}
          value={email}
          onChangeText={(val) => setEmail(val)}
          errors={pathOr([], ["email"], validationErrors)}
        />
        <TextField
          placeholder="..."
          labelTx="editProfileScreen.name"
          autoCapitalize="none"
          autoCorrect={false}
          returnKeyType="next"
          onSubmitEditing={() => refPasswordField.current.focus()}
          blurOnSubmit={false}
          forwardedRef={refNameField}
          style={{ marginBottom: spacing[4] }}
          value={name}
          onChangeText={(val) => setName(val)}
          errors={pathOr([], ["name"], validationErrors)}
        />
        <TextField
          placeholder="******"
          textContentType="oneTimeCode"
          labelTx="signInScreen.password"
          secureTextEntry={true}
          autoCapitalize="none"
          autoCorrect={false}
          returnKeyType="done"
          onSubmitEditing={() => onSubmit()}
          blurOnSubmit={false}
          forwardedRef={refPasswordField}
          value={password}
          onChangeText={(val) => setPassword(val)}
          errors={pathOr([], ["password"], validationErrors)}
        />
        <Button
          preset="pink"
          tx="signInScreen.signUp"
          textTransform="uppercase"
          appendIcon="arrow-right"
          style={SUBMIT}
          loading={processing}
          onPress={onSubmit}
        />
      </View>
    </Screen>
  )
})
