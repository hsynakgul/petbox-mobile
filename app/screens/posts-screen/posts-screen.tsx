import React, { useEffect, useRef, useState } from "react"
import { observer } from "mobx-react-lite"
import {
  Dimensions,
  FlatList,
  Image,
  ImageStyle,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { NotFound, Screen, ScreenRefProps, Text } from "../../components"
import { useNavigation, useRoute } from "@react-navigation/native"
import { color, spacing } from "../../theme"
import { useStores } from "../../models"
import { GetPostsResult, Post } from "../../services/api"
import { pathOr } from "ramda"
import { sizes } from "../../theme/sizes"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

const CONTAINER: ViewStyle = {
  backgroundColor: color.palette.white,
}

const CONTENT_CONTAINER: ViewStyle = {
  paddingRight: spacing[4],
  paddingBottom: spacing[6],
  flex: 1,
}

const ITEM: ViewStyle = {
  width: Dimensions.get("window").width / 2 - spacing[4] * 1.5,
  position: "relative",
  marginLeft: spacing[4],
  backgroundColor: color.borderLight,
  borderRadius: 35,
  borderBottomLeftRadius: 20,
  borderBottomRightRadius: 20,
  marginBottom: spacing[3],
}

const ITEM_DETAIL: ViewStyle = {
  width: "100%",
  paddingBottom: spacing[3],
}

const ITEM_IMAGE: ImageStyle = {
  width: "100%",
  aspectRatio: 1,
  resizeMode: "cover",
  borderRadius: 25,
}

const ITEM_TITLE: TextStyle = {
  textAlign: "left",
  paddingVertical: spacing[2],
  paddingHorizontal: spacing[4],
  fontSize: sizes.medium,
  fontWeight: "bold",
}

const OTHER_TEXT: TextStyle = {
  textAlign: "left",
  paddingHorizontal: spacing[4],
  fontSize: sizes.small,
}

export function PostItem({ navigation, item }) {
  const goToDetail = (postId) => {
    navigation.navigate("postDetail", {
      postId,
    })
  }

  const coverPhotoPath = pathOr(null, ["photos", 0, "small"], item)
  return (
    <TouchableOpacity
      style={[ITEM, { opacity: item.status === 1 ? 1 : 0.5 }]}
      key={"post-" + item.id}
      onPress={() => goToDetail(item.id)}
    >
      <Image source={{ uri: coverPhotoPath }} style={ITEM_IMAGE} />
      <View style={ITEM_DETAIL}>
        <Text text={item.title} style={ITEM_TITLE} numberOfLines={2} />
        <Text text={`${item.city} - ${item.district}`} style={OTHER_TEXT} numberOfLines={1} />
        <Text
          text={`${item.created_at}`}
          style={[OTHER_TEXT, { marginTop: spacing[1] }]}
          numberOfLines={1}
        />
      </View>
    </TouchableOpacity>
  )
}

export const PostsScreen = observer(function PostsScreen() {
  const { postStore } = useStores()
  const navigation = useNavigation()
  const route = useRoute()
  const routeParams: Record<string, any> = route.params

  // region refs
  const refScreen = useRef<ScreenRefProps>(null)
  // endregion

  // region states
  const [refreshing, setRefreshing] = useState<boolean>(false)
  const [posts, setPosts] = useState<Post[]>([])
  // endregion

  const fetchPosts = () => {
    setRefreshing(true)
    postStore
      .fetch({
        category_id: routeParams?.category_id,
      })
      .then((rsp: GetPostsResult) => {
        if (rsp.kind === "ok") {
          setPosts(rsp.data)
        }
      })
      .finally(() => {
        setRefreshing(false)
      })
  }

  useEffect(() => {
    fetchPosts()
    navigation.addListener("focus", () => {
      fetchPosts()
    })
  }, [])

  // regions renderFunctions
  const _renderList = () => {
    return (
      <>
        {posts.length > 0 ? (
          <View style={{ paddingHorizontal: spacing[5], paddingVertical: spacing[5] }}>
            <Text text={`Toplam ${posts.length} ilan bulunuyor.`} />
          </View>
        ) : null}
        <FlatList
          data={posts}
          renderItem={(ctx) => PostItem({ ...ctx, navigation })}
          horizontal={false}
          onScroll={(event) => refScreen.current.handleScroll(event)}
          scrollEventThrottle={10}
          numColumns={2}
          refreshing={refreshing}
          onRefresh={fetchPosts}
          keyExtractor={(item, index) => `${item.id}-${index.toString()}`}
          style={CONTAINER}
          contentContainerStyle={CONTENT_CONTAINER}
          ListEmptyComponent={_renderNotFound()}
        />
      </>
    )
  }
  const _renderNotFound = () => {
    return <NotFound text="Aradığınız kriterlere ait ilan bulamadık" />
  }
  // endregion

  // Pull in navigation via hook
  return (
    <Screen
      ref={refScreen}
      style={ROOT}
      preset="fixed"
      rounded={true}
      header={{
        theme: "primary",
        headerTx: "postsScreen.headerTitle",
      }}
      paddingTop={0}
      statusBarBg={color.primary}
    >
      {_renderList()}
    </Screen>
  )
})
