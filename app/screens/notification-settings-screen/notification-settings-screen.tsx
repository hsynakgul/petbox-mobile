import React, { useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { View, ViewStyle } from "react-native"
import { Button, Screen, Switch, Text } from "../../components"
import { useStores } from "../../models"
import { color, spacing } from "../../theme"
import { Profile } from "../../services/api"
import { useNavigation } from "@react-navigation/native"
import { sizes } from "../../theme/sizes"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

const CONTAINER: ViewStyle = {
  paddingHorizontal: spacing[5],
}

const SUBMIT: ViewStyle = {
  marginTop: spacing[6],
}

export const NotificationSettingsScreen = observer(function NotificationSettingsScreen() {
  const { authStore } = useStores()
  const navigation = useNavigation()
  // region states
  const [processing, setProcessing] = useState<boolean>(false)
  const [query, setQuery] = useState<Profile>({
    send_email: 0,
    send_notification: 0,
    send_sms: 0,
  })
  // endregion

  // region refs
  useEffect(() => {
    setQuery(authStore.profile)
  }, [])
  // endregion

  // region functions
  const onSubmit = () => {
    if (processing) return null
    setProcessing(true)
    authStore.update(query).then((rsp) => {
      if (rsp.kind === "ok") {
        navigation.goBack()
      } else {
        setProcessing(false)
      }
    })
  }
  // endregion

  return (
    <Screen
      style={ROOT}
      preset="scroll"
      rounded={true}
      visibleStatusBar={false}
      header={{ headerTx: "notificationSettingsScreen.headerTitle" }}
    >
      <View style={CONTAINER}>
        <View style={{ marginBottom: spacing[6] }}>
          <Switch
            labelTx="notificationSettingsScreen.pushNotification"
            fontWeightBold={true}
            value={query.send_notification === 1}
            onToggle={(val) => setQuery({ ...query, send_notification: val ? 1 : 0 })}
          />
          <Text
            tx="notificationSettingsScreen.pushNote"
            style={{ fontSize: sizes.medium, marginTop: spacing[3] }}
          />
        </View>
        <View style={{ marginBottom: spacing[6] }}>
          <Switch
            labelTx="notificationSettingsScreen.emailNotification"
            fontWeightBold={true}
            value={query.send_email === 1}
            onToggle={(val) => setQuery({ ...query, send_email: val ? 1 : 0 })}
          />
          <Text
            tx="notificationSettingsScreen.emailNote"
            style={{ fontSize: sizes.medium, marginTop: spacing[3] }}
          />
        </View>
        <View style={{ marginBottom: spacing[4] }}>
          <Switch
            labelTx="notificationSettingsScreen.smsNotification"
            fontWeightBold={true}
            value={query.send_sms === 1}
            onToggle={(val) => setQuery({ ...query, send_sms: val ? 1 : 0 })}
          />
          <Text
            tx="notificationSettingsScreen.smsNote"
            style={{ fontSize: sizes.medium, marginTop: spacing[3] }}
          />
        </View>
        <Button
          tx="common.save"
          textTransform="uppercase"
          style={SUBMIT}
          loading={processing}
          onPress={onSubmit}
        />
      </View>
    </Screen>
  )
})
