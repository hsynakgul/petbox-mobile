import React, { useEffect, useRef, useState } from "react"
import { observer } from "mobx-react-lite"
import { TextInput, View, ViewStyle } from "react-native"
import { Button, Screen, TextFieldPhone, TextField, SelectableList } from "../../components"
import { useStores } from "../../models"
import { color, spacing } from "../../theme"
import { pathOr } from "ramda"
import { Profile } from "../../services/api"
import { useNavigation } from "@react-navigation/native"
import { TextInputMasked } from "react-native-masked-text"
import { BottomSheetModalProvider } from "@gorhom/bottom-sheet"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

const CONTAINER: ViewStyle = {
  paddingHorizontal: spacing[5],
  paddingBottom: spacing[10],
}

const SUBMIT: ViewStyle = {
  marginTop: spacing[6],
}

export const EditProfileScreen = observer(function EditProfileScreen() {
  const { authStore } = useStores()
  const navigation = useNavigation()
  // region states
  const [processing, setProcessing] = useState<boolean>(false)
  const [query, setQuery] = useState<Profile>({
    name: "",
    email: "",
    gender: 0,
    phone: "",
  })
  const [validationErrors, setValidationErrors] = useState<object>({})
  // endregion

  // region refs
  useEffect(() => {
    setQuery(authStore.profile)
  }, [])
  // endregion

  // region refs
  const refNameField = useRef<TextInput>(null)
  const refEmailField = useRef<TextInput>(null)
  const refPhoneField = useRef<TextInputMasked>(null)
  // endregion

  // region functions
  const onSubmit = () => {
    if (processing) return null
    setProcessing(true)
    setValidationErrors({})
    authStore.update(query).then((rsp) => {
      if (rsp.kind === "ok") {
        navigation.goBack()
      } else {
        setProcessing(false)
        if (rsp.kind === "validation") {
          setValidationErrors(rsp.validation || {})
        }
      }
    })
  }
  // endregion

  const genderOptions = [
    {
      value: 1,
      text: "Erkek",
    },
    {
      value: 2,
      text: "Kadın",
    },
    {
      value: 0,
      text: "Belirtmek istemiyorum",
    },
  ]

  return (
    <BottomSheetModalProvider>
      <Screen
        style={ROOT}
        preset="scroll"
        rounded={true}
        visibleStatusBar={false}
        header={{ headerTx: "editProfileScreen.headerTitle" }}
      >
        <View style={CONTAINER}>
          <TextField
            placeholder="..."
            labelTx="editProfileScreen.name"
            autoCapitalize="none"
            autoCorrect={false}
            returnKeyType="next"
            onSubmitEditing={() => refEmailField.current.focus()}
            blurOnSubmit={false}
            forwardedRef={refNameField}
            style={{ marginBottom: spacing[4] }}
            value={query.name}
            onChangeText={(val) => setQuery((prevState) => ({ ...prevState, name: val }))}
            errors={pathOr([], ["name"], validationErrors)}
          />
          <TextField
            placeholder="example@mail.com"
            labelTx="editProfileScreen.email"
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
            returnKeyType="next"
            onSubmitEditing={() => refPhoneField.current.getElement().focus()}
            blurOnSubmit={false}
            forwardedRef={refEmailField}
            style={{ marginBottom: spacing[4] }}
            value={query.email}
            onChangeText={(val) => setQuery((prevState) => ({ ...prevState, email: val }))}
            errors={pathOr([], ["email"], validationErrors)}
          />
          <TextFieldPhone
            labelTx="editProfileScreen.phone"
            returnKeyType="done"
            onSubmitEditing={() => refPhoneField.current.getElement().blur()}
            blurOnSubmit={false}
            forwardedRef={refPhoneField}
            style={{ marginBottom: spacing[4] }}
            value={query.phone}
            onChangeText={(val) => setQuery((prevState) => ({ ...prevState, phone: val }))}
            errors={pathOr([], ["phone"], validationErrors)}
          />
          <SelectableList
            labelTx="editProfileScreen.gender"
            options={genderOptions}
            value={query.gender}
            onChange={(val: number) => setQuery((prevState) => ({ ...prevState, gender: val }))}
            errors={pathOr([], ["gender"], validationErrors)}
          />
          <Button
            tx="common.save"
            textTransform="uppercase"
            style={SUBMIT}
            loading={processing}
            onPress={onSubmit}
          />
        </View>
      </Screen>
    </BottomSheetModalProvider>
  )
})
