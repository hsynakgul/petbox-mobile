import React, { useEffect } from "react"
import { observer } from "mobx-react-lite"
import { SafeAreaView, TextStyle, View, ViewStyle } from "react-native"
import LottieView from "lottie-react-native"
import { Screen, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
import { useStores } from "../../models"
import { spacing } from "../../theme"
import { widthPercentageToDP } from "react-native-responsive-screen"

const ROOT: ViewStyle = {
  flex: 1,
}

const CONTAINER: ViewStyle = {
  flex: 1,
  justifyContent: "space-between",
  alignItems: "center",
  paddingVertical: spacing[5],
}

const TEXT: TextStyle = {
  textAlign: "center",
  fontSize: 12,
}

export const LoadingScreen = observer(function LoadingScreen() {
  // Pull in one of our MST stores
  const { authStore, systemStore, categoryStore, chatStore } = useStores()
  // OR
  // const rootStore = useStores()

  // Pull in navigation via hook
  // const navigation = useNavigation()
  useEffect(() => {
    authStore.getProfile().then((rsp) => {
      if (rsp.kind === "ok") {
        categoryStore.fetch().finally(() => {
          chatStore.fetch().finally(() => {
            authStore.getFavorites().finally(() => {
              systemStore.setLoading(false)
            })
          })
        })
      } else if (rsp.kind === "unauthorized") {
        authStore.logout()
      }
    })
  }, [])
  return (
    <Screen style={ROOT} preset="fixed">
      <SafeAreaView style={ROOT}>
        <View style={CONTAINER}>
          <LottieView
            style={{ width: widthPercentageToDP("120%") }}
            source={require("../../../assets/animations/profile_loading.json")}
            autoPlay={true}
            loop={true}
          />
          <Text tx="loadingScreen.applicationLoading" style={TEXT} />
        </View>
      </SafeAreaView>
    </Screen>
  )
})
