import React, { useEffect, useMemo, useRef, useState } from "react"
import { observer } from "mobx-react-lite"
import {
  Dimensions,
  Image,
  ImageStyle,
  RefreshControl,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { Alert, AlertRefProps, Button, CarouselScreen, Text } from "../../components"
import { color, spacing } from "../../theme"
import { useNavigation, useRoute } from "@react-navigation/native"
import Carousel, { Pagination } from "react-native-snap-carousel"
import { File, GetPostResult, Post } from "../../services/api"
import { useStores } from "../../models"
import { find, findIndex, pathOr, propEq } from "ramda"
import { sizes } from "../../theme/sizes"
import { Icon } from "../../utils/icons"
import { translate } from "../../i18n"
const { width: viewportWidth } = Dimensions.get("window")

const IMAGE: ImageStyle = {
  width: "100%",
  height: undefined,
  aspectRatio: 1,
}

const TITLE: TextStyle = {
  fontSize: sizes.header,
  fontWeight: "bold",
  marginBottom: spacing[4],
}

const PASSIVE_TEXT: TextStyle = {
  fontSize: sizes.header,
  fontWeight: "bold",
  color: color.palette.white,
}

const PASSIVE_CONTAINER: ViewStyle = {
  width: "100%",
  height: "100%",
  position: "absolute",
  backgroundColor: color.blackDarkenTransparent,
  alignItems: "center",
  justifyContent: "center",
  top: 0,
  left: 0,
  zIndex: 1,
}

const agesList = require("../../services/api/static/ages.json")
const sexList = require("../../services/api/static/sex.json")

export const PostDetailScreen = observer(function PostDetailScreen() {
  const { postStore, toastStore, authStore, chatStore } = useStores()
  const navigation = useNavigation()
  const route = useRoute()
  const routeParams: Record<string, any> = route.params

  const alertRef = useRef<AlertRefProps>(null)

  // region states
  const [refreshing, setRefreshing] = useState<boolean>(true)
  const [post, setPost] = useState<Post>({})
  const [photos, setPhotos] = useState<File[]>([])
  const [activeSlide, setActiveSlide] = useState<number>(0)
  // endregion

  const fetchPost = () => {
    setRefreshing(true)
    postStore
      .get(routeParams?.postId)
      .then((rsp: GetPostResult) => {
        if (rsp.kind === "ok") {
          setPost(rsp.data)
          setPhotos(pathOr([], ["photos"], rsp.data))
        } else {
          toastStore.pushToast({
            text: rsp.message,
            type: "error",
          })
        }
      })
      .finally(() => {
        setRefreshing(false)
      })
  }

  useEffect(() => {
    fetchPost()
  }, [routeParams?.postId])

  const _renderCarouselItem = ({ item, index }) => {
    return (
      <View>
        {post.status === 0 ? (
          <View style={PASSIVE_CONTAINER}>
            <Text tx="addPostScreen.passiveText" style={PASSIVE_TEXT} />
          </View>
        ) : null}
        <Image source={{ uri: item.large }} style={IMAGE} />
      </View>
    )
  }

  const _renderCarousel = () => {
    return (
      <>
        <Carousel
          data={pathOr([], ["photos"], post)}
          renderItem={_renderCarouselItem}
          sliderWidth={viewportWidth}
          itemWidth={viewportWidth}
          slideStyle={{
            width: viewportWidth,
          }}
          containerCustomStyle={{
            overflow: "hidden",
          }}
          inactiveSlideOpacity={1}
          inactiveSlideScale={1}
          onSnapToItem={(index) => setActiveSlide(index)}
        />
        {_renderPagination()}
      </>
    )
  }

  const _renderPagination = () => {
    return (
      <Pagination
        dotsLength={photos.length}
        activeDotIndex={activeSlide}
        containerStyle={{
          position: "absolute",
          width: "100%",
          top: viewportWidth.valueOf() - 100,
        }}
        dotStyle={{
          width: 15,
          height: 15,
          borderRadius: 10,
          marginHorizontal: 0,
          backgroundColor: color.palette.pink,
          borderWidth: 2,
          borderColor: color.palette.white,
        }}
        dotContainerStyle={{ marginHorizontal: 3 }}
        inactiveDotStyle={
          {
            // Define styles for inactive dots here
          }
        }
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    )
  }

  const ageText = useMemo(() => {
    const index = findIndex(propEq("value", post.age), agesList)
    return pathOr("", [index, "text"], agesList)
  }, [post.age])

  const sexText = useMemo(() => {
    const index = findIndex(propEq("value", post.sex), sexList)
    return pathOr("", [index, "text"], sexList)
  }, [post.sex])

  const toggleLike = () => {
    const value = post.favorite === 1 ? 0 : 1
    setPost((prevState) => ({ ...prevState, favorite: value }))
    postStore.favorite(routeParams.postId, value).then((rsp) => {
      if (rsp.kind !== "ok") {
        setPost((prevState) => ({ ...prevState, favorite: value === 1 ? 0 : 1 }))
        toastStore.pushToast({
          title: "common.error",
          text: rsp.message,
          type: "error",
        })
      }
    })
  }

  const pressDestroy = () => {
    alertRef.current.open({
      titleTx: "addPostScreen.confirmDestroy",
      buttons: [
        {
          tx: "common.delete",
          textStyle: { color: color.error },
          onPress: () => {
            postStore.destroy(routeParams.postId).then((rsp) => {
              if (rsp.kind !== "ok") {
                toastStore.pushToast({
                  title: "common.error",
                  text: rsp.message,
                  type: "error",
                })
              } else {
                toastStore.pushToast({
                  title: translate("addPostScreen.successfulDestroy"),
                  type: "success",
                })
                navigation.goBack()
              }
            })
          },
        },
        {
          tx: "common.cancel",
        },
      ],
    })
  }

  const pressChangeStatus = () => {
    const status = post.status === 1 ? 0 : 1
    console.tron.log(status)
    alertRef.current.open({
      titleTx: "addPostScreen.confirmChangeStatus",
      buttons: [
        {
          tx: "common.apply",
          textStyle: { color: color.error },
          onPress: () => {
            postStore.changeStatus(routeParams.postId, { status }).then((rsp) => {
              if (rsp.kind !== "ok") {
                toastStore.pushToast({
                  title: "common.error",
                  text: rsp.message,
                  type: "error",
                })
              } else {
                setPost((prevState) => ({ ...prevState, status }))
              }
            })
          },
        },
        {
          tx: "common.cancel",
        },
      ],
    })
  }

  const sendMessage = () => {
    let chatId = null
    chatStore.list.map((item) => {
      const has = find(propEq("id", post.user_id), item.users)
      if (has && !chatId) chatId = item.id
    })
    navigation.navigate("chatRoom", { chatId, fromUsers: [post.user_id] })
  }

  const _renderActionButtons = () => {
    if (post.user_id !== authStore.profile.id)
      return (
        <View
          style={{
            marginTop: spacing[4],
            paddingTop: spacing[4],
            borderTopWidth: 1,
            borderColor: color.borderLight,
            flexDirection: "row",
          }}
        >
          <Button
            preset="pink"
            style={{ paddingHorizontal: spacing[4], flex: 1 }}
            onPress={sendMessage}
          >
            <Icon name="send" size={24} color={color.palette.white} />
            <Text
              text="Mesaj Gönder"
              style={{
                color: color.palette.white,
                paddingLeft: spacing[2],
                fontSize: sizes.header,
              }}
            />
          </Button>
        </View>
      )
    return (
      <View
        style={{
          marginTop: spacing[4],
          paddingTop: spacing[4],
          borderTopWidth: 1,
          borderColor: color.borderLight,
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <Button
          preset={post.status === 1 ? "orange" : "blue"}
          style={{ paddingHorizontal: spacing[4] }}
          onPress={pressChangeStatus}
        >
          <Icon name={post.status === 1 ? "hide" : "show"} size={24} color={color.palette.white} />
          <Text
            text={post.status === 1 ? "Yayından Kaldır" : "Yayına Al"}
            style={{ color: color.palette.white, paddingLeft: spacing[2], fontSize: sizes.header }}
          />
        </Button>
        <Button preset="pink" style={{ paddingHorizontal: spacing[4] }} onPress={pressDestroy}>
          <Icon name="delete" size={24} color={color.palette.white} />
          <Text
            text="İlanı Sil"
            style={{ color: color.palette.white, paddingLeft: spacing[2], fontSize: sizes.header }}
          />
        </Button>
      </View>
    )
  }

  const _renderEditButton = () => {
    if (post.user_id !== authStore.profile.id)
      return (
        <TouchableOpacity onPress={toggleLike}>
          <Icon
            name="heart"
            size={32}
            color={post.favorite === 1 ? color.palette.pink : color.text}
          />
        </TouchableOpacity>
      )
    return (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate("editPostScreen", { postId: routeParams?.postId })
        }}
      >
        <Icon name="edit" size={32} color={color.text} />
      </TouchableOpacity>
    )
  }

  return (
    <CarouselScreen
      carousel={_renderCarousel()}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={fetchPost} style={{ zIndex: 20 }} />
      }
    >
      <Alert ref={alertRef} />
      <Text style={TITLE} text={pathOr("", ["title"], post)} />
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <View style={{ flex: 1 }}>
          <Text text={pathOr("", ["user", "name"], post)} style={{marginBottom: spacing[2], fontSize: sizes.header}} />
          <Text text={pathOr("", ["created_at"], post)} />
          <View style={{ flexDirection: "row", marginVertical: spacing[1] }}>
            <Text text={pathOr("", ["city"], post)} />
            <Text text="/" style={{ marginHorizontal: spacing[1] }} />
            <Text text={pathOr("", ["district"], post)} />
          </View>
        </View>
        <View>{_renderEditButton()}</View>
      </View>
      <View
        style={{
          marginTop: spacing[4],
          paddingTop: spacing[4],
          borderTopWidth: 1,
          borderColor: color.borderLight,
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
            <Icon name="category" size={22} color={color.primary} />
            <Text
              text={pathOr("", ["category", "title"], post)}
              style={{ fontSize: sizes.header, marginLeft: spacing[2] }}
            />
          </View>
          <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
            <Icon name="ticket-star" size={22} color={color.palette.pink} />
            <Text
              text={pathOr("", ["breed", "title"], post)}
              style={{ fontSize: sizes.header, marginLeft: spacing[2] }}
            />
          </View>
        </View>
        <View style={{ flexDirection: "row", marginTop: spacing[3] }}>
          <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
            <Icon name="bookmark" size={22} color={color.palette.orangeDarker} />
            <Text tx={sexText} style={{ fontSize: sizes.header, marginLeft: spacing[2] }} />
          </View>
          <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
            <Icon name="time-circle" size={22} color={color.palette.lightGrey} />
            <Text tx={ageText} style={{ fontSize: sizes.header, marginLeft: spacing[2] }} />
          </View>
        </View>
      </View>
      <View
        style={{
          marginTop: spacing[4],
          paddingTop: spacing[4],
          borderTopWidth: 1,
          borderColor: color.borderLight,
        }}
      >
        <Text text={pathOr("", ["description"], post)} />
      </View>
      {_renderActionButtons()}
    </CarouselScreen>
  )
})
