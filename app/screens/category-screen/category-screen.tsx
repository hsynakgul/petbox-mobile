import React, { useCallback, useEffect, useRef, useState } from "react"
import { observer } from "mobx-react-lite"
import {
  FlatList,
  ImageStyle,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
  Dimensions,
} from "react-native"
import { Icon, Screen, ScreenRefProps, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, spacing } from "../../theme"
import { Category, GetCategoriesResult } from "../../services/api"
import { useStores } from "../../models"
import { useNavigation } from "@react-navigation/native"

const ROOT: ViewStyle = {
  flex: 1,
}

const CONTAINER: ViewStyle = {
  backgroundColor: color.palette.white,
}

const CONTENT_CONTAINER: ViewStyle = {
  paddingHorizontal: spacing[3],
  paddingTop: spacing[3],
  paddingBottom: spacing[6],
}

const ITEM: ViewStyle = {
  backgroundColor: color.borderLight,
  width: Dimensions.get("window").width / 2 - spacing[3] * 3,
  margin: spacing[3],
  alignItems: "center",
  justifyContent: "center",
  borderRadius: spacing[3],
  height: 175,
  padding: spacing[3],
}

const ITEM_IMAGE: ImageStyle = {
  width: 80,
  height: 80,
}

const ITEM_TEXT: TextStyle = {
  textAlign: "center",
  marginTop: spacing[4],
}

export const CategoryScreen = observer(function CategoryScreen() {
  const { categoryStore } = useStores()
  const navigation = useNavigation()

  // region states
  const [refreshing, setRefreshing] = useState<boolean>(false)
  const [categories, setCategories] = useState<Category[]>([])
  // endregion

  const fetchCategories = useCallback(() => {
    setRefreshing(true)
    categoryStore.fetch().then((rsp: GetCategoriesResult) => {
      setRefreshing(false)
      if (rsp.kind === "ok") {
        setCategories(rsp.data)
      }
    })
  }, [])

  useEffect(() => {
    fetchCategories()
  }, [])

  // useEffect(() => {
  //   if (categoryStore.list) {
  //     setCategories(categoryStore.list.slice())
  //   }
  // }, [categoryStore.list])

  // region refs
  const refScreen = useRef<ScreenRefProps>(null)
  // endregion

  const goToPosts = (id) => {
    navigation.navigate("posts", {
      category_id: id,
    })
  }

  // regions renderFunctions
  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity style={ITEM} key={"category-" + item.id} onPress={() => goToPosts(item.id)}>
        <Icon icon={("category_" + item.id) as "category_1"} style={ITEM_IMAGE} />
        <Text text={item.title} style={ITEM_TEXT} />
      </TouchableOpacity>
    )
  }
  // endregion

  return (
    <Screen
      ref={refScreen}
      style={ROOT}
      preset="fixed"
      rounded={true}
      header={{
        theme: "primary",
        headerTx: "categoryScreen.headerTitle",
        rightIcon: "search",
      }}
      statusBarBg={color.primary}
    >
      <FlatList
        data={categories}
        renderItem={renderItem}
        numColumns={2}
        horizontal={false}
        onScroll={(event) => refScreen.current.handleScroll(event)}
        scrollEventThrottle={10}
        refreshing={refreshing}
        onRefresh={fetchCategories}
        keyExtractor={(item, index) => `${item.id}-${index.toString()}`}
        style={CONTAINER}
        contentContainerStyle={CONTENT_CONTAINER}
      />
    </Screen>
  )
})
