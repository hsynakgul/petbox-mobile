import React from "react"
import { observer } from "mobx-react-lite"
import { Alert, Image, ImageStyle, TextStyle, View, ViewStyle } from "react-native"
import { heightPercentageToDP as hp } from "react-native-responsive-screen"
import { Button, Screen, Text, TextField } from "../../components"
import { color, spacing } from "../../theme"

const IMAGE_SRC = require("../../../assets/image/forgot-password.png")

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

const FORM_CONTAINER: ViewStyle = {
  paddingHorizontal: spacing[5],
  paddingBottom: spacing[5],
  paddingTop: spacing[0]
}

const SUBMIT: ViewStyle = {
  marginTop: spacing[6],
}

const IMAGE_CONTAINER: ViewStyle = {
  alignItems: "center",
}

const IMAGE: ImageStyle = {
  backgroundColor: color.background,
  resizeMode: "contain",
  height: hp("35%"),
}

const MESSAGE: TextStyle = {
  color: color.text,
  fontSize: 15,
  marginBottom: spacing[6],
  textAlign: "center",
}

export const ForgotPasswordScreen = observer(function ForgotPasswordScreen() {
  const onSubmit = () => {
    Alert.alert("Mail gönderilecek")
  }
  return (
    <Screen
      style={ROOT}
      preset="scroll"
      statusBar="dark-content"
      header={{ headerTx: "forgotPasswordScreen.headerTitle", theme: "white" }}
    >
      <View style={IMAGE_CONTAINER}>
        <Image source={IMAGE_SRC} style={IMAGE} />
      </View>
      <View style={FORM_CONTAINER}>
        <Text
          style={MESSAGE}
          tx="forgotPasswordScreen.note"
        />
        <TextField
          placeholder="example@mail.com"
          labelTx="editProfileScreen.email"
          keyboardType="email-address"
          autoCapitalize="none"
          autoCorrect={false}
          returnKeyType="done"
          onSubmitEditing={() => onSubmit()}
          blurOnSubmit={false}
          style={{ marginBottom: spacing[4] }}
        />
        <Button
          preset="orange"
          tx="common.send"
          textTransform="uppercase"
          style={SUBMIT}
          onPress={onSubmit}
        />
      </View>
    </Screen>
  )
})
