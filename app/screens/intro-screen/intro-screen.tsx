import React, { useRef, useState } from "react"
import { observer } from "mobx-react-lite"
import {
  Dimensions,
  Image,
  ImageStyle,
  SafeAreaView,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { Text } from "../../components"
import { color, spacing } from "../../theme"
import AppIntroSlider from "react-native-app-intro-slider"
import { useStores } from "../../models"

const WINDOW_WIDTH = Dimensions.get("window").width

const CONTAINER: ViewStyle = {
  backgroundColor: color.background,
  flex: 1,
}

const IMAGE: ImageStyle = {
  backgroundColor: color.background,
  resizeMode: "stretch",
  width: WINDOW_WIDTH,
  flex: 2,
}

const CONTENT: ViewStyle = {
  backgroundColor: color.background,
  paddingHorizontal: spacing[5],
  paddingVertical: spacing[8],
  flex: 1,
}

const TITLE: TextStyle = {
  fontSize: 26,
  fontWeight: "bold",
  textAlign: "center",
  marginBottom: spacing[4],
}

const TEXT: TextStyle = {
  fontSize: 16,
  textAlign: "center",
  marginBottom: spacing[5],
}

const BUTTON_VIEW: ViewStyle = {
  paddingVertical: spacing[2],
  paddingHorizontal: spacing[4],
}

const BUTTON_TEXT: TextStyle = {
  fontSize: 18,
  fontWeight: "bold",
}

const PAGINATION_CONTAINER: ViewStyle = {
  position: "absolute",
  bottom: 16,
  left: 16,
  right: 16,
}

const PAGINATION_BUTTONS: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
}

const PAGINATION_DOTS: ViewStyle = {
  height: 16,
  margin: 16,
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
}

const PAGINATION_DOT: ViewStyle = {
  width: 10,
  height: 10,
  borderRadius: 5,
  marginHorizontal: 4,
  borderWidth: 1,
}

export const IntroScreen = observer(function IntroScreen() {
  const { systemStore } = useStores()
  const introRef = useRef<AppIntroSlider>(null)
  const [slides] = useState([
    {
      key: "one",
      title: "Maecenas ante quam",
      text:
        "Phasellus non tempor sapien, quis posuere risus. Maecenas in tempor eros. Morbi ultricies sed nunc ac interdum. Duis pretium sapien non risus mollis iaculis. Morbi eget ex et ligula bibendum ultricies.",
      image: require("../../../assets/image/intro/intro-one.png"),
      backgroundColor: color.palette.blue,
    },
    {
      key: "two",
      title: "Integer quis purus malesuada",
      text:
        "Donec laoreet, leo at viverra dignissim, tellus turpis semper urna, id porttitor ante elit vehicula lectus.",
      image: require("../../../assets/image/intro/intro-two.png"),
      backgroundColor: color.palette.orange,
    },
    {
      key: "three",
      title: "Nunc vehicula purus",
      text:
        "Nam libero ligula, posuere porta quam at, dignissim interdum massa. Nulla facilisi. Mauris rutrum, " +
        "urna sed dapibus bibendum, sem leo laoreet nulla, et convallis nisl turpis sed ante. Donec quis ex id " +
        "quam pulvinar auctor eget eget massa. Morbi diam lacus, molestie at ante ut, tempus efficitur dolor.",
      image: require("../../../assets/image/intro/intro-three.png"),
      backgroundColor: color.palette.pink,
    },
  ])

  // region renderFunctions
  const renderItem = ({ item }) => {
    return (
      <View style={CONTAINER}>
        <Image source={item.image} style={IMAGE} />
        <View style={CONTENT}>
          <Text style={TITLE} transform="capitalize">
            {item.title}
          </Text>
          <Text style={TEXT}>{item.text}</Text>
        </View>
      </View>
    )
  }
  const renderPagination = (activeIndex) => {
    return (
      <View style={PAGINATION_CONTAINER}>
        <SafeAreaView>
          <View style={PAGINATION_DOTS}>
            {slides.length > 1 &&
              slides.map((_, i) => (
                <TouchableOpacity
                  key={i}
                  style={[
                    PAGINATION_DOT,
                    i === activeIndex
                      ? {
                          backgroundColor: _.backgroundColor,
                          borderColor: slides[activeIndex].backgroundColor,
                        }
                      : {
                          backgroundColor: color.palette.white,
                          borderColor: slides[activeIndex].backgroundColor,
                        },
                  ]}
                />
              ))}
          </View>
          <View style={PAGINATION_BUTTONS}>
            {renderSkipButton()}
            {renderNextButton()}
            {renderDoneButton()}
          </View>
        </SafeAreaView>
      </View>
    )
  }
  const renderNextButton = () => {
    if (!introRef.current) return null
    const activeIndex = introRef.current.state.activeIndex
    if (activeIndex + 1 >= slides.length) return null
    return (
      <TouchableOpacity
        style={BUTTON_VIEW}
        onPress={() => introRef.current.goToSlide(activeIndex + 1)}
      >
        <Text style={BUTTON_TEXT} tx="common.next" transform="uppercase" />
      </TouchableOpacity>
    )
  }
  const renderSkipButton = () => {
    if (!introRef.current) return null
    const activeIndex = introRef.current.state.activeIndex
    if (activeIndex + 1 >= slides.length) return <View />
    return (
      <TouchableOpacity style={BUTTON_VIEW} onPress={() => onDone()}>
        <Text style={BUTTON_TEXT} tx="common.skip" transform="uppercase" />
      </TouchableOpacity>
    )
  }
  const renderDoneButton = () => {
    if (!introRef.current) return null
    const activeIndex = introRef.current.state.activeIndex
    if (activeIndex + 1 < slides.length) return null
    return (
      <TouchableOpacity style={BUTTON_VIEW} onPress={() => onDone()}>
        <Text style={BUTTON_TEXT} tx="common.done" transform="uppercase" />
      </TouchableOpacity>
    )
  }
  // endregion

  // region functions
  const onDone = () => {
    systemStore.setIntro(false)
  }
  // endregion

  return (
    <AppIntroSlider
      ref={introRef}
      data={slides}
      renderItem={renderItem}
      renderPagination={renderPagination}
      onDone={onDone}
    />
  )
})
