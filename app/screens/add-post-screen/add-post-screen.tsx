import React, { useEffect, useMemo, useState } from "react"
import { observer } from "mobx-react-lite"
import { View, ViewStyle } from "react-native"
import { Screen, SelectableList, SelectPhotos, TextField } from "../../components"
import { color, spacing } from "../../theme"
import { findIndex, pathOr, propEq } from "ramda"
import { GetPostResult, Post } from "../../services/api"
import { useStores } from "../../models"
import { translate } from "../../i18n"
import { useNavigation, useRoute } from "@react-navigation/native"
import { BottomSheetModalProvider } from "@gorhom/bottom-sheet"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

const CONTAINER: ViewStyle = {
  flex: 1,
  paddingBottom: spacing[8],
}

const citiesJSON: { name: string; id: string }[] = require("../../services/api/static/cities.json")
const districtsJSON: {
  id: string
  city_id: string
  name: string
}[] = require("../../services/api/static/districts.json")
const agesList = require("../../services/api/static/ages.json")
const sexList = require("../../services/api/static/sex.json")

export const AddPostScreen = observer(function AddPostScreen() {
  const { categoryStore, postStore, toastStore } = useStores()
  const navigation = useNavigation()
  const route = useRoute()
  const routeParams: Record<string, any> = route.params

  const [processing, setProcessing] = useState<boolean>(false)
  const [uploading, setUploading] = useState<boolean>(false)

  const [query, setQuery] = useState<Post>({
    title: "",
    description: "",
    age: 1,
    sex: 0,
    category_id: null,
    breed_id: null,
    city: "",
    district: "",
    photos: [],
  })

  useEffect(() => {
    if (routeParams?.postId) {
      postStore
        .get(routeParams?.postId)
        .then((rsp: GetPostResult) => {
          if (rsp.kind === "ok") {
            setQuery(rsp.data)
          } else {
            toastStore.pushToast({
              text: rsp.message,
              type: "error",
            })
          }
        })
        .finally(() => {})
    }
  }, [routeParams?.postId])

  const categories = useMemo(() => {
    return categoryStore.list.map((item) => ({ value: item.id, text: `${item.title}` }))
  }, [])

  const breeds = useMemo(() => {
    const arr = pathOr(
      [],
      [findIndex(propEq("id", query.category_id), categoryStore.list), "breeds"],
      categoryStore.list,
    )
    return arr.map((item) => ({ value: item.id, text: `${item.title}` }))
  }, [query.category_id])

  const cities = useMemo(() => {
    return citiesJSON.map((item) => ({ value: item.name, text: `(${item.id}) ${item.name}` }))
  }, [])

  const districts = useMemo(() => {
    if (!query.city) return []
    const selectedCityId = pathOr(
      null,
      [findIndex(propEq("name", query.city), citiesJSON), "id"],
      citiesJSON,
    )
    return districtsJSON
      .filter((item) => item.city_id === selectedCityId)
      .map((item) => ({ value: item.name, text: `${item.name}` }))
  }, [query.city])

  const [validationErrors, setValidationErrors] = useState<object>({})

  const onSubmit = () => {
    if (processing || uploading) return null
    setProcessing(true)
    setValidationErrors({})
    if (routeParams?.postId) return onUpdate()
    postStore.store(query).then((rsp) => {
      setProcessing(false)
      if (rsp.kind !== "ok") {
        if (rsp.kind === "validation") {
          setValidationErrors(rsp.validation || {})
        }
        toastStore.pushToast({
          title: rsp.message,
          type: "error",
        })
      } else {
        toastStore.pushToast({
          title: translate("addPostScreen.successful_save"),
          type: "success",
        })
        navigation.navigate("profile")
      }
    })
  }

  const onUpdate = () => {
    postStore.update(routeParams?.postId, query).then((rsp) => {
      setProcessing(false)
      if (rsp.kind !== "ok") {
        if (rsp.kind === "validation") {
          setValidationErrors(rsp.validation || {})
        }
        toastStore.pushToast({
          title: rsp.message,
          type: "error",
        })
      } else {
        toastStore.pushToast({
          title: translate("addPostScreen.successful_save"),
          type: "success",
        })
        navigation.navigate("profile")
      }
    })
  }

  const renderForms = () => {
    return (
      <>
        <TextField
          placeholderTx="addPostScreen.title_example"
          labelTx="addPostScreen.title"
          autoCorrect={false}
          returnKeyType="next"
          blurOnSubmit={false}
          value={query.title}
          onChangeText={(val) => setQuery((prevState) => ({ ...prevState, title: val }))}
          errors={pathOr([], ["title"], validationErrors)}
          hideErrorDetail={true}
          preset="horizontal"
        />
        <SelectableList
          key="category-list"
          preset="horizontal"
          labelTx="addPostScreen.category"
          options={categories}
          value={query.category_id}
          onChange={(val: number) =>
            setQuery((prevState) => ({ ...prevState, category_id: val, breed_id: null }))
          }
          errors={pathOr([], ["category_id"], validationErrors)}
          hideErrorDetail={true}
        />
        <SelectableList
          key="breed-list"
          preset="horizontal"
          labelTx="addPostScreen.breed"
          options={breeds}
          value={query.breed_id}
          onChange={(val: number) => setQuery((prevState) => ({ ...prevState, breed_id: val }))}
          errors={pathOr([], ["breed_id"], validationErrors)}
          hideErrorDetail={true}
        />
        <SelectableList
          key="gender-list"
          preset="horizontal"
          labelTx="addPostScreen.sex"
          options={sexList}
          value={query.sex}
          onChange={(val: number) => setQuery((prevState) => ({ ...prevState, sex: val }))}
          errors={pathOr([], ["sex"], validationErrors)}
          hideErrorDetail={true}
        />
        <SelectableList
          key="age-list"
          preset="horizontal"
          labelTx="addPostScreen.age"
          options={agesList}
          value={query.age}
          onChange={(val: number) => setQuery((prevState) => ({ ...prevState, age: val }))}
          errors={pathOr([], ["age"], validationErrors)}
          hideErrorDetail={true}
        />
        <SelectableList
          key="city-select-list"
          preset="horizontal"
          labelTx="addPostScreen.city"
          options={cities}
          value={query.city}
          onChange={(val: string) =>
            setQuery((prevState) => ({ ...prevState, city: val, district: "" }))
          }
          errors={pathOr([], ["city"], validationErrors)}
          hideErrorDetail={true}
        />
        <SelectableList
          key="district-select-list"
          preset="horizontal"
          labelTx="addPostScreen.district"
          options={districts}
          value={query.district}
          onChange={(val: string) => setQuery((prevState) => ({ ...prevState, district: val }))}
          errors={pathOr([], ["district"], validationErrors)}
          hideErrorDetail={true}
        />
        <TextField
          placeholderTx="addPostScreen.write_description_here"
          labelTx="addPostScreen.description"
          returnKeyType="done"
          blurOnSubmit={false}
          value={query.description}
          onChangeText={(val) => setQuery((prevState) => ({ ...prevState, description: val }))}
          errors={pathOr([], ["description"], validationErrors)}
          preset="horizontal"
          multiline={true}
          hideErrorDetail={true}
        />
      </>
    )
  }

  return (
    <BottomSheetModalProvider>
      <Screen
        style={ROOT}
        preset="scroll"
        rounded={true}
        header={{
          theme: "orange",
          headerTx: routeParams?.postId
            ? "addPostScreen.editHeaderTitle"
            : "addPostScreen.headerTitle",
          rightIcon: "tick-square",
          rightText: "common.save",
          onRightPress() {
            onSubmit()
          },
        }}
        statusBarBg={color.palette.orangeDarker}
        paddingTop={spacing[2]}
      >
        <View style={CONTAINER}>
          <SelectPhotos
            uploading={uploading}
            setUploading={(val) => setUploading(val)}
            photos={query.photos}
            setPhotos={(val) => setQuery((prevState) => ({ ...prevState, photos: val }))}
            errors={pathOr([], ["photos"], validationErrors)}
          />
          {renderForms()}
        </View>
      </Screen>
    </BottomSheetModalProvider>
  )
})
