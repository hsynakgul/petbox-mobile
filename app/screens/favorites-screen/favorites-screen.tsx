import React, { useEffect, useRef, useState } from "react"
import { observer } from "mobx-react-lite"
import { FlatList, View, ViewStyle } from "react-native"
import { NotFound, Screen, ScreenRefProps, Text } from "../../components"
import { useNavigation } from "@react-navigation/native"
import { color, spacing } from "../../theme"
import { useStores } from "../../models"
import { GetPostsResult, Post } from "../../services/api"
import { PostItem } from "../posts-screen/posts-screen"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

const CONTAINER: ViewStyle = {
  backgroundColor: color.palette.white,
}

const CONTENT_CONTAINER: ViewStyle = {
  paddingRight: spacing[4],
  paddingBottom: spacing[6],
  flex: 1
}

export const FavoritesScreen = observer(function FavoritesScreen() {
  const { authStore } = useStores()
  const navigation = useNavigation()

  // region refs
  const refScreen = useRef<ScreenRefProps>(null)
  // endregion

  // region states
  const [refreshing, setRefreshing] = useState<boolean>(false)
  const [posts, setPosts] = useState<Post[]>(authStore.favorites.slice(0))
  // endregion

  const fetchPosts = () => {
    setRefreshing(true)
    authStore
      .getFavorites()
      .then((rsp: GetPostsResult) => {
        if (rsp.kind === "ok") {
          setPosts(rsp.data)
        }
      })
      .finally(() => {
        setRefreshing(false)
      })
  }

  useEffect(() => {
    fetchPosts()
  }, [])

  // regions renderFunctions
  const _renderList = () => {
    return (
      <>
        {posts.length > 0 ? (
          <View style={{ paddingHorizontal: spacing[5], paddingVertical: spacing[5] }}>
            <Text text={`Favorilerinizde ${posts.length} ilan bulunuyor.`} />
          </View>
        ) : null}
        <FlatList
          data={posts}
          renderItem={(ctx) => PostItem({ ...ctx, navigation })}
          horizontal={false}
          onScroll={(event) => refScreen.current.handleScroll(event)}
          scrollEventThrottle={10}
          numColumns={2}
          refreshing={refreshing}
          onRefresh={fetchPosts}
          keyExtractor={(item, index) => `${item.id}-${index.toString()}`}
          style={CONTAINER}
          contentContainerStyle={CONTENT_CONTAINER}
          ListEmptyComponent={_renderNotFound()}
        />
      </>
    )
  }
  const _renderNotFound = () => {
    return <NotFound text="Favorilerinize eklenmiş bir ilan yok"/>
  }
  // endregion

  // Pull in navigation via hook
  return (
    <Screen
      ref={refScreen}
      style={ROOT}
      preset="fixed"
      rounded={true}
      header={{
        theme: "pink",
        headerTx: "favoritesScreen.headerTitle",
      }}
      paddingTop={0}
      statusBarBg={color.palette.pink}
    >
      {_renderList()}
    </Screen>
  )
})
