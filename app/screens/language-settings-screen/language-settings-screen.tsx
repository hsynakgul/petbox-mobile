import React from "react"
import { observer } from "mobx-react-lite"
import { TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { Screen, Text } from "../../components"
import { color, spacing } from "../../theme"
import { i18n, setLanguageTag, useSystemLanguage } from "../../i18n"
import { sizes } from "../../theme/sizes"
import { pathOr } from "ramda"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

const SELECTABLE_ITEM: ViewStyle = {
  paddingVertical: spacing[4],
  paddingHorizontal: spacing[5],
  borderBottomWidth: 1,
  borderColor: color.borderLight,
  flexDirection: "row",
  alignItems: "center",
}

const SELECTABLE_ITEM_BADGE: ViewStyle = {
  width: 14,
  height: 14,
  borderColor: color.borderLight,
  backgroundColor: color.palette.lighterGrey,
  borderWidth: 2,
  borderRadius: 12,
  marginRight: spacing[3],
}

const SELECTABLE_ITEM_TEXT: TextStyle = {
  fontSize: sizes.header,
}

export const LanguageSettingsScreen = observer(function LanguageSettingsScreen() {
  // region renderFunctions
  const renderDefaultItem = () => {
    const badgeStyle = [SELECTABLE_ITEM_BADGE]
    if (useSystemLanguage) badgeStyle.push({ backgroundColor: color.primary })
    return (
      <TouchableOpacity
        style={SELECTABLE_ITEM}
        onPress={() => {
          setLanguageTag(null)
        }}
      >
        <View style={badgeStyle} />
        <Text
          tx={"settingsScreen.useSystemLanguage"}
          transform="capitalize"
          style={SELECTABLE_ITEM_TEXT}
        />
      </TouchableOpacity>
    )
  }
  const renderSelectableItem = (key) => {
    const isActive = key === i18n.locale
    const badgeStyle = [SELECTABLE_ITEM_BADGE]
    if (isActive && !useSystemLanguage) badgeStyle.push({ backgroundColor: color.primary })
    return (
      <TouchableOpacity
        style={SELECTABLE_ITEM}
        onPress={() => {
          setLanguageTag(key)
        }}
        key={key}
      >
        <View style={badgeStyle} />
        <Text
          text={pathOr("", [key, "languageText"], i18n.translations)}
          style={SELECTABLE_ITEM_TEXT}
        />
      </TouchableOpacity>
    )
  }
  // endregion
  return (
    <Screen
      style={ROOT}
      preset="scroll"
      rounded={true}
      visibleStatusBar={false}
      header={{ headerTx: "settingsScreen.languageSettings" }}
    >
      {renderDefaultItem()}
      {Object.keys(i18n.translations).map((key) => renderSelectableItem(key))}
    </Screen>
  )
})
