import React, { useRef } from "react"
import { observer } from "mobx-react-lite"
import { ImageStyle, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { Screen, Text, Icon as IconPNG, Alert, AlertRefProps } from "../../components"
import { useNavigation } from "@react-navigation/native"
import { useStores } from "../../models"
import { color, spacing } from "../../theme"
import { Icon } from "../../utils/icons"
import { sizes } from "../../theme/sizes"

const ROOT: ViewStyle = {
  flex: 1,
}

const ACCOUNT_SUMMARY: ViewStyle = {
  paddingHorizontal: spacing[5],
  paddingBottom: spacing[4],
  borderBottomWidth: 1,
  borderColor: color.borderLight,
  marginBottom: spacing[3],
}

const NAME: TextStyle = {
  fontSize: sizes.mediumPlus,
  marginBottom: spacing[1],
}

const EMAIL: TextStyle = {
  fontSize: sizes.medium,
  marginBottom: spacing[1],
}

const SMALL_TEXT: TextStyle = {
  fontSize: sizes.small,
}

const ITEM: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  paddingHorizontal: spacing[5],
  paddingVertical: spacing[3],
}

const ITEM_ICON: ImageStyle = {
  width: 48,
  height: 48,
}

const ITEM_TEXT: TextStyle = {
  flex: 1,
  fontSize: sizes.medium,
  marginHorizontal: spacing[4],
}

export const SettingsScreen = observer(function SettingsScreen() {
  const alertRef = useRef<AlertRefProps>(null)
  const { authStore } = useStores()
  const navigation = useNavigation()
  const logout = () => {
    alertRef.current.open({
      titleTx: "settingsScreen.confirmLogout",
      buttons: [
        {
          tx: "settingsScreen.signOut",
          textStyle: { color: color.error },
          onPress: () => {
            authStore.logout()
          },
        },
        {
          tx: "common.cancel",
        },
      ],
    })
  }

  return (
    <Screen
      style={ROOT}
      preset="scroll"
      rounded={true}
      header={{ headerTx: "settingsScreen.headerTitle", theme: "primary" }}
      statusBarBg={color.primary}
    >
      <Alert ref={alertRef} />
      <View style={ACCOUNT_SUMMARY}>
        <Text style={NAME} text={authStore.profile.name} />
        <Text style={EMAIL} text={authStore.profile.email} />
        {authStore.profile.email_is_valid === 0 ? (
          <Text
            tx="settingsScreen.emailIsInvalid"
            style={[SMALL_TEXT, { color: color.palette.pink }]}
          />
        ) : null}
      </View>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate("editProfile")
        }}
      >
        <View style={ITEM}>
          <IconPNG icon="account" style={ITEM_ICON} />
          <Text style={ITEM_TEXT} tx="editProfileScreen.headerTitle" />
          <Icon name="arrow-right" size={20} color={color.text} />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate("notificationSettings")
        }}
      >
        <View style={ITEM}>
          <IconPNG icon="notification" style={ITEM_ICON} />
          <Text style={ITEM_TEXT} tx="notificationSettingsScreen.headerTitle" />
          <Icon name="arrow-right" size={20} color={color.text} />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate("languageSettings")
        }}
      >
        <View style={ITEM}>
          <IconPNG icon="voice" style={ITEM_ICON} />
          <Text style={ITEM_TEXT} tx="settingsScreen.languageSettings" />
          <Icon name="arrow-right" size={20} color={color.text} />
        </View>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => logout()}>
        <View style={ITEM}>
          <IconPNG icon="logout" style={ITEM_ICON} />
          <Text style={ITEM_TEXT} tx="settingsScreen.signOut" />
        </View>
      </TouchableOpacity>
    </Screen>
  )
})
