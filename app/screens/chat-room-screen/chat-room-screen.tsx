import React, { useEffect, useRef, useState } from "react"
import { observer } from "mobx-react-lite"
import { View, ViewStyle } from "react-native"
import { Alert, AlertRefProps, Screen } from "../../components"
import { color } from "../../theme"
import { Bubble, GiftedChat, IMessage, Send } from "react-native-gifted-chat"
import { Icon } from "../../utils/icons"
import { useNavigation, useRoute } from "@react-navigation/native"
import { useStores } from "../../models"
import { findIndex, pathOr, propEq } from "ramda"
import { ChatMessage, GetChatResult } from "../../services/api"
import { onSnapshot } from "mobx-state-tree"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

export const ChatRoomScreen = observer(function ChatRoomScreen() {
  const { chatStore, authStore } = useStores()
  const navigation = useNavigation()
  const { params: routeParams } = useRoute()
  const chatId = useRef(pathOr(null, ["chatId"], routeParams))
  const alertRef = useRef<AlertRefProps>(null)

  const chatFind = () => {
    if (!chatId.current) return null
    const index = findIndex(propEq("id", chatId.current), chatStore.list)
    return pathOr(null, [index], chatStore.list)
  }

  const [chat, setChat] = useState(chatFind())

  const getUserName = (userId) => {
    const users = pathOr([], ["users"], chat)
    const index = findIndex(propEq("id", userId), users)
    return pathOr("", [index, "name"], users)
  }

  const formatMessages = (messages: ChatMessage[]) => {
    return messages.map((message) => {
      return {
        _id: message.id,
        text: message.text,
        user: { _id: message.user_id, name: getUserName(message.user_id) },
        createdAt: Date.parse(message.created_at),
      } as IMessage
    })
  }

  const [messages, setMessages] = useState(formatMessages(chat?.messages.slice(0) || []))

  const createChat = () => {
    chatStore
      .createChat({
        from_users: pathOr([], ["fromUsers"], routeParams),
      })
      .then((rsp: GetChatResult) => {
        if (rsp.kind === "ok") {
          chatId.current = rsp.data.id
          setChat(chatFind())
          alertRef.current.close()
        }
      })
  }

  const handleSend = (newMessage = []) => {
    if (chat) {
      chat.sendMessage({
        text: pathOr("", [0, "text"], newMessage),
      })
    }
    setMessages(GiftedChat.append(messages, newMessage))
  }

  // @ts-ignore
  useEffect(() => {
    if (!chatId.current) {
      alertRef.current.open({
        titleTx: "chatRoomScreen.startChat",
        noClose: true,
        buttons: [
          {
            tx: "common.start",
            textStyle: { color: color.primary },
            onPress: () => createChat(),
          },
          {
            tx: "common.cancel",
            onPress: () => {
              alertRef.current.close()
              navigation.goBack()
            },
          },
        ],
      })
    }
    if (chat) {
      return onSnapshot(chat, () => {
        setMessages(formatMessages(chat?.messages.slice(0) || []))
      })
    }
  }, [chat])

  const _renderSend = (props) => {
    return (
      <Send {...props}>
        <View style={{ marginRight: 10, marginBottom: 7 }}>
          <Icon name="send" size={28} color={color.text} />
        </View>
      </Send>
    )
  }

  const renderBubble = (props) => <Bubble {...props} />

  return (
    <Screen
      style={ROOT}
      preset="fixed"
      rounded={false}
      header={{
        theme: "purple",
        headerText: pathOr("", ["users", 0, "name"], chat)
      }}
      paddingTop={0}
      statusBarBg={color.palette.purple}
      behavior={null}
    >
      <Alert ref={alertRef} />
      <GiftedChat
        messages={messages}
        onSend={(newMessage) => handleSend(newMessage)}
        user={{ _id: authStore.id }}
        alwaysShowSend
        placeholder="Mesajınızı yazın..."
        renderSend={_renderSend}
        renderBubble={renderBubble}
      />
    </Screen>
  )
})
