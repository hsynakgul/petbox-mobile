import React, { useEffect, useRef, useState } from "react"
import { observer } from "mobx-react-lite"
import { FlatList, TextStyle, View, ViewStyle } from "react-native"
import { NotFound, Screen, ScreenRefProps, Text } from "../../components"
import { useNavigation } from "@react-navigation/native"
import { color, spacing } from "../../theme"
import { useStores } from "../../models"
import { GetPostsResult, Post } from "../../services/api"
import { PostItem } from "../posts-screen/posts-screen"
import { sizes } from "../../theme/sizes"
import { pathOr } from "ramda"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

const CONTAINER: ViewStyle = {
  backgroundColor: color.palette.white,
}

const CONTENT_CONTAINER: ViewStyle = {
  paddingRight: spacing[4],
  paddingBottom: spacing[6],
  flex: 1,
}

const NUMBER_CARDS: ViewStyle = {
  flexDirection: "row",
  marginBottom: spacing[5],
}

const NUMBER_CARD: ViewStyle = {
  flex: 1,
  paddingHorizontal: spacing[3],
  paddingVertical: spacing[5],
  backgroundColor: color.palette.inputAddonBg,
}

const NUMBER: TextStyle = {
  textAlign: "center",
  width: "100%",
  fontSize: sizes.huge,
  fontWeight: "bold",
}

const NUMBER_TEXT: TextStyle = {
  textAlign: "center",
  width: "100%",
  fontSize: sizes.mediumPlus,
}

export const ProfileScreen = observer(function ProfileScreen() {
  const { authStore, chatStore } = useStores()
  const navigation = useNavigation()

  // region refs
  const refScreen = useRef<ScreenRefProps>(null)
  // endregion

  // region states
  const [refreshing, setRefreshing] = useState<boolean>(true)
  const [posts, setPosts] = useState<Post[]>(authStore.posts.data.slice(0))
  const [postsMeta, setPostsMeta] = useState<Record<string, any>>(authStore.posts.data.meta)

  const fetchPosts = () => {
    setRefreshing(true)
    authStore
      .getPosts()
      .then((rsp: GetPostsResult) => {
        if (rsp.kind === "ok") {
          setPosts(rsp.data)
          setPostsMeta(rsp.meta)
        }
      })
      .finally(() => {
        setRefreshing(false)
      })
  }

  useEffect(() => {
    fetchPosts()
  }, [])

  const _renderList = () => {
    return (
      <>
        <FlatList
          data={posts}
          renderItem={(ctx) => PostItem({ ...ctx, navigation })}
          horizontal={false}
          onScroll={(event) => refScreen.current.handleScroll(event)}
          scrollEventThrottle={10}
          numColumns={2}
          refreshing={refreshing}
          onRefresh={fetchPosts}
          keyExtractor={(item, index) => `${item.id}-${index.toString()}`}
          style={CONTAINER}
          contentContainerStyle={CONTENT_CONTAINER}
          ListEmptyComponent={_renderNotFound()}
        />
      </>
    )
  }
  const _renderNotFound = () => {
    return <NotFound text="Henüz ilan eklemediniz" />
  }

  // Pull in navigation via hook
  return (
    <Screen
      ref={refScreen}
      style={ROOT}
      preset="fixed"
      rounded={true}
      header={{
        theme: "primary",
        headerTx: "profileScreen.myPosts",
        rightIcon: "setting",
        onRightPress() {
          navigation.navigate("settings")
        },
      }}
      paddingTop={0}
      statusBarBg={color.primary}
    >
      <View style={NUMBER_CARDS}>
        <View style={NUMBER_CARD}>
          <Text style={NUMBER}>{pathOr(0, ["total"], postsMeta)}</Text>
          <Text style={NUMBER_TEXT}>İlan</Text>
        </View>
        <View style={NUMBER_CARD}>
          <Text style={NUMBER}>{authStore.favorites.length}</Text>
          <Text style={NUMBER_TEXT}>Favori</Text>
        </View>
        <View style={NUMBER_CARD}>
          <Text style={NUMBER}>{chatStore.list.length}</Text>
          <Text style={NUMBER_TEXT}>Mesajlaşma</Text>
        </View>
      </View>
      {_renderList()}
    </Screen>
  )
})
