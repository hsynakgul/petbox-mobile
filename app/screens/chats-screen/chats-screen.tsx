import React, { useEffect, useRef, useState } from "react"
import { observer } from "mobx-react-lite"
import { FlatList, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { NotFound, Screen, ScreenRefProps, Text } from "../../components"
import { color, spacing } from "../../theme"
import { useNavigation } from "@react-navigation/native"
import { useStores } from "../../models"
import { Chat } from "../../services/api"
import { pathOr } from "ramda"
import { sizes } from "../../theme/sizes"
import { onSnapshot } from "mobx-state-tree"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

const CONTAINER: ViewStyle = {
  backgroundColor: color.palette.white,
}

const CONTENT_CONTAINER: ViewStyle = {
  flex: 1,
}

const CHAT: ViewStyle = {
  paddingHorizontal: spacing[4],
  paddingVertical: spacing[3],
  borderBottomWidth: 1,
  borderColor: color.borderLight,
  flexDirection: "row",
  alignItems: "center",
}

const CHAT_AVATAR: ViewStyle = {
  width: 60,
  height: 60,
  borderRadius: 30,
  alignItems: "center",
  justifyContent: "center",
  backgroundColor: color.borderLight,
}

const CHAT_DETAIL: ViewStyle = {
  flex: 1,
  paddingHorizontal: spacing[4],
  paddingVertical: spacing[2],
}

const CHAT_NAME: TextStyle = {
  fontSize: sizes.header,
  marginBottom: spacing[1],
}

const CHAT_TEXT: TextStyle = {
  fontSize: sizes.mediumPlus,
}

const CHAT_DATE: TextStyle = {
  fontSize: sizes.small,
  marginBottom: spacing[2],
}

const AVATAR_TEXT: TextStyle = {
  fontSize: sizes.headerIcon,
}

export const ChatsScreen = observer(function ChatsScreen() {
  const { chatStore } = useStores()
  const navigation = useNavigation()
  // region refs
  const refScreen = useRef<ScreenRefProps>(null)
  const refFlatList = useRef<FlatList>(null)
  // endregion

  const [refreshing, setRefreshing] = useState<boolean>(false)
  const [chats, setChats] = useState<Chat[]>([])

  const fetchChats = () => {
    setRefreshing(true)
    chatStore.fetch().finally(() => {
      setRefreshing(false)
    })
  }

  useEffect(() => {
    fetchChats()
    return onSnapshot(chatStore, () => {
      setChats(chatStore.allChats.slice(0))
      if (refFlatList.current) refFlatList.current.forceUpdate()
    })
  }, [])

  const _renderItem = ({ item }) => {
    const name = pathOr("...", ["users", 0, "name"], item)
    const acronym = name.split(/\s/).reduce((response, word) => (response += word.slice(0, 1)), "")
    return (
      <TouchableOpacity
        onPress={() => navigation.navigate("chatRoom", { chatId: item.id })}
        style={CHAT}
      >
        <View style={CHAT_AVATAR}>
          <Text style={AVATAR_TEXT} text={acronym} />
        </View>
        <View style={CHAT_DETAIL}>
          <Text style={CHAT_NAME} text={name} />
          <Text style={CHAT_DATE} text={pathOr("...", ["messages", 0, "created_at_text"], item)} />
          <Text
            style={CHAT_TEXT}
            numberOfLines={2}
            text={pathOr("...", ["messages", 0, "text"], item)}
          />
        </View>
      </TouchableOpacity>
    )
  }

  // regions renderFunctions
  const _renderList = () => {
    return (
      <>
        <FlatList
          ref={refFlatList}
          data={chats}
          renderItem={_renderItem}
          horizontal={false}
          onScroll={(event) => refScreen.current.handleScroll(event)}
          scrollEventThrottle={10}
          refreshing={refreshing}
          onRefresh={fetchChats}
          keyExtractor={(item, index) => `${item.id}-${index.toString()}`}
          style={CONTAINER}
          ListEmptyComponent={_renderNotFound()}
          contentContainerStyle={CONTENT_CONTAINER}
        />
      </>
    )
  }

  const _renderNotFound = () => {
    return <NotFound text="Henüz mesajlaşmanız yok" />
  }
  // endregion

  return (
    <Screen
      ref={refScreen}
      style={ROOT}
      preset="fixed"
      rounded={true}
      header={{
        theme: "purple",
        headerTx: "chatsScreen.headerTitle",
      }}
      paddingTop={0}
      statusBarBg={color.palette.purple}
    >
      {_renderList()}
    </Screen>
  )
})
