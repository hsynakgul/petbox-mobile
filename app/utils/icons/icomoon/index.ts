import { createIconSetFromIcoMoon } from "react-native-vector-icons"
// @ts-ignore
import icomoonConfig from './selection.json'
const Icon = createIconSetFromIcoMoon(icomoonConfig)
export default Icon
