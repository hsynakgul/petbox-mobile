/**
 * This is the navigator you will modify to display the logged-in screens of your app.
 * You can use RootNavigator to also display an auth flow or other user flows.
 *
 * You'll likely spend most of your time in this file.
 */
import React from "react"

import {
  AddPostScreen,
  CategoryScreen,
  ChatRoomScreen,
  ChatsScreen,
  EditProfileScreen,
  FavoritesScreen,
  LanguageSettingsScreen,
  NotificationSettingsScreen,
  PostDetailScreen,
  PostsScreen,
  ProfileScreen,
  SettingsScreen,
} from "../screens"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { CustomTabBar } from "./custom-tab-bar"
import { Icon } from "../utils/icons"
import { createNativeStackNavigator } from "react-native-screens/native-stack"

/**
 * This type allows TypeScript to know what routes are defined in this navigator
 * as well as what properties (if any) they might take when navigating to them.
 *
 * If no params are allowed, pass through `undefined`. Generally speaking, we
 * recommend using your MobX-State-Tree store(s) to keep application state
 * rather than passing state through navigation params.
 *
 * For more information, see this documentation:
 *   https://reactnavigation.org/docs/params/
 *   https://reactnavigation.org/docs/typescript#type-checking-the-navigator
 */

const Category = createNativeStackNavigator()
const CategoryStack = () => (
  <Category.Navigator screenOptions={{ headerShown: false }} initialRouteName="category">
    <Category.Screen name="category" component={CategoryScreen} />
    <Category.Screen name="posts" component={PostsScreen} />
    <Category.Screen name="postDetail" component={PostDetailScreen} />
    <Category.Screen name="editPostScreen" component={AddPostScreen} />
  </Category.Navigator>
)

const Profile = createNativeStackNavigator()
const ProfileStack = () => (
  <Profile.Navigator screenOptions={{ headerShown: false }} initialRouteName="profile">
    <Profile.Screen name="profile" component={ProfileScreen} />
    <Profile.Screen name="settings" component={SettingsScreen} />
    <Profile.Screen
      name="editProfile"
      component={EditProfileScreen}
      options={{ stackPresentation: "modal" }}
    />
    <Profile.Screen
      name="notificationSettings"
      component={NotificationSettingsScreen}
      options={{ stackPresentation: "modal" }}
    />
    <Profile.Screen
      name="languageSettings"
      component={LanguageSettingsScreen}
      options={{ stackPresentation: "modal" }}
    />
    <Profile.Screen name="postDetail" component={PostDetailScreen} />
    <Profile.Screen name="editPostScreen" component={AddPostScreen} />
  </Profile.Navigator>
)

// Documentation: https://github.com/software-mansion/react-native-screens/tree/master/native-stack
const Tab = createBottomTabNavigator()

const TabNavigator = () => {
  return (
    <Tab.Navigator
      backBehavior="none"
      initialRouteName="category"
      tabBar={(props) => <CustomTabBar {...props} />}
    >
      <Tab.Screen
        name="category"
        options={{ tabBarIcon: (props) => <Icon name="home" {...props} /> }}
        component={CategoryStack}
      />
      <Tab.Screen
        name="favorites"
        options={{ unmountOnBlur: true, tabBarIcon: (props) => <Icon name="heart" {...props} /> }}
        component={FavoritesScreen}
      />
      <Tab.Screen
        name="addPost"
        options={{ unmountOnBlur: true, tabBarIcon: (props) => <Icon name="plus" {...props} /> }}
        component={AddPostScreen}
      />
      <Tab.Screen
        name="chats"
        options={{ unmountOnBlur: true, tabBarIcon: (props) => <Icon name="chat" {...props} /> }}
        component={ChatsScreen}
      />
      <Tab.Screen
        name="profile"
        options={{ unmountOnBlur: true, tabBarIcon: (props) => <Icon name="profile" {...props} /> }}
        component={ProfileStack}
      />
    </Tab.Navigator>
  )
}

const Primary = createNativeStackNavigator()
export function PrimaryNavigator() {
  return (
    <Primary.Navigator screenOptions={{ headerShown: false }}>
      <Primary.Screen name="main" component={TabNavigator} />
      <Primary.Screen name="chatRoom" component={ChatRoomScreen} />
    </Primary.Navigator>
  )
}

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 *
 * `canExit` is used in ./app/app.tsx in the `useBackButtonHandler` hook.
 */
const exitRoutes = ["main"]
export const canExit = (routeName: string) => exitRoutes.includes(routeName)
