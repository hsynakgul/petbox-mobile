import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Dimensions, ViewStyle } from "react-native"
import { Svg, Path } from 'react-native-svg';
import { line, curveBasis } from 'd3-shape';
import { widthPercentageToDP as wp } from "react-native-responsive-screen"
import { color } from "../theme"

const CONTAINER: ViewStyle = {
  shadowColor: color.dim,
  shadowOffset: { width: 0, height: -3 },
  shadowRadius: 4,
  shadowOpacity: 0.1,
}

const { width: wWidth } = Dimensions.get('window');
const lineGenerator = line()
  .x(({ x }) => x)
  .y(({ y }) => y);
const curveLineGenerator = line()
  .curve(curveBasis)
  .x(({ x }) => x)
  .y(({ y }) => y);

function TabShape({ tabWidth, tabHeight }) {
  const d = useMemo(() => {
    const left = lineGenerator([
      { x: 0, y: 0 },
      { x: tabWidth * 2, y: 0 },
    ]);
    const center = curveLineGenerator([
      { x: tabWidth * 2 - wp('10%'), y: 0 },
      { x: tabWidth * 2 - wp('1%'), y: 0 },
      { x: tabWidth * 2 + wp('3%'), y: tabHeight * 0.64 },
      { x: tabWidth * 3 - wp('3%'), y: tabHeight * 0.64 },
      { x: tabWidth * 3 + wp('1%'), y: 0 },
      { x: tabWidth * 3 + wp('10%'), y: 0 },
    ]);
    const right = lineGenerator([
      { x: tabWidth * 3, y: 0 },
      { x: wWidth, y: 0 },
      { x: wWidth, y: tabHeight },
      { x: 0, y: tabHeight },
      { x: 0, y: 0 },
    ]);
    return `${left} ${center} ${right}`;
  }, [tabWidth]);

  return (
    <Svg style={CONTAINER} width={wWidth} {...{ height: tabHeight }}>
      <Path fill={color.palette.white} {...{ d }} />
    </Svg>
  );
}

TabShape.propTypes = {
  tabWidth: PropTypes.number.isRequired,
  tabHeight: PropTypes.number.isRequired,
};

export default TabShape;
