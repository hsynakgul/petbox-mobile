/**
 * The root navigator is used to switch between major navigation flows of your app.
 * Generally speaking, it will contain an auth flow (registration, login, forgot password)
 * and a "main" flow (which is contained in your PrimaryNavigator) which the user
 * will use once logged in.
 */
import React from "react"
import { NavigationContainer, NavigationContainerRef, StackActions } from "@react-navigation/native"

import { createNativeStackNavigator } from "react-native-screens/native-stack"
import { AuthNavigator } from "./auth-navigator"
import { PrimaryNavigator } from "./primary-navigator"
import { useStores } from "../models"
import { IntroScreen, LoadingScreen } from "../screens"
import { observer } from "mobx-react-lite"
import { addMiddleware } from "mobx-state-tree"

/**
 * This type allows TypeScript to know what routes are defined in this navigator
 * as well as what properties (if any) they might take when navigating to them.
 *
 * We recommend using MobX-State-Tree store(s) to handle state rather than navigation params.
 *
 * For more information, see this documentation:
 *   https://reactnavigation.org/docs/params/
 *   https://reactnavigation.org/docs/typescript#type-checking-the-navigator
 */
export type RootParamList = {
  loading: undefined
  authStack: undefined
  primaryStack: undefined
}

const Stack = createNativeStackNavigator<RootParamList>()

// @ts-ignore
const RootStack = observer((props: { containerRef: any }) => {
  const { authStore, systemStore } = useStores()
  addMiddleware(authStore, (call, next) => {
    if (call.name === "logout") {
      if (props.containerRef.current && props.containerRef.current.canGoBack()) {
        props.containerRef.current.dispatch(StackActions.popToTop())
      }
    }
    next(call)
  })
  if (systemStore.intro) {
    return <IntroScreen />
  }
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
      }}
    >
      {authStore.token ? (
        systemStore.loading ? (
          <Stack.Screen
            name="loading"
            component={LoadingScreen}
            options={{
              headerShown: false,
              stackAnimation: "fade",
            }}
          />
        ) : (
          <Stack.Screen
            name="primaryStack"
            component={PrimaryNavigator}
            options={{
              headerShown: false,
              stackAnimation: "fade",
            }}
          />
        )
      ) : (
        <Stack.Screen
          name="authStack"
          component={AuthNavigator}
          options={{
            headerShown: false,
            stackAnimation: "fade",
          }}
        />
      )}
    </Stack.Navigator>
  )
})

export const RootNavigator = React.forwardRef<
  NavigationContainerRef,
  Partial<React.ComponentProps<typeof NavigationContainer>>
>((props, ref) => {
  return (
    <NavigationContainer {...props} ref={ref}>
      <RootStack containerRef={ref} />
    </NavigationContainer>
  )
})

RootNavigator.displayName = "RootNavigator"
