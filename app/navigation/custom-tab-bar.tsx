import * as React from "react"
import { Dimensions, SafeAreaView, TouchableOpacity, View, ViewStyle } from "react-native"
import { color } from "../theme"
import { flatten, mergeAll } from "ramda"
import TabShape from "./custom-tab-shape"
const { width: wWidth } = Dimensions.get("window")

const CONTAINER: ViewStyle = {
  position: "relative",
  height: 64
}

const TABS: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "flex-end",
  height: 64,
  position: "absolute",
  bottom: 0,
  zIndex: 1,
  width: "100%",
}

const TAB_ITEM: ViewStyle = {
  flex: 1,
  justifyContent: "center",
  alignItems: "center",
  height: 64,
  position: "relative",
}

const MIDDLE_TAB_ITEM: ViewStyle = {
  backgroundColor: color.palette.white,
  borderRadius: 32,
  width: 64,
  flex: 0,
  transform: [
    {translateY: -32}
  ],
  shadowColor: color.dim,
  shadowOffset: { width: 0, height: 0 },
  shadowRadius: 5,
  shadowOpacity: 0.2,
  elevation: 4,
}

/**
 * Describe your component here
 */
export function CustomTabBar({ state, descriptors, navigation }) {
  const focusedOptions = descriptors[state.routes[state.index].key].options

  if (focusedOptions.tabBarVisible === false) {
    return null
  }

  const renderRouteItem = (route, index) => {
    const { options } = descriptors[route.key]
    const icon = options.tabBarIcon !== undefined ? options.tabBarIcon : null

    const isFocused = state.index === index

    const onPress = () => {
      const event = navigation.emit({
        type: "tabPress",
        target: route.key,
        canPreventDefault: true,
      })

      if (!isFocused && !event.defaultPrevented) {
        navigation.navigate(route.name)
      }
    }

    const onLongPress = () => {
      navigation.emit({
        type: "tabLongPress",
        target: route.key,
      })
    }

    const styles = [TAB_ITEM]
    if (index === 2) styles.push(MIDDLE_TAB_ITEM)
    const viewStyles = mergeAll(flatten(styles))

    return (
      <View style={{flex: 1, alignItems: "center"}} key={route.key}>
        <TouchableOpacity
          accessibilityRole="button"
          accessibilityState={isFocused ? { selected: true } : {}}
          accessibilityLabel={options.tabBarAccessibilityLabel}
          testID={options.tabBarTestID}
          onPress={onPress}
          onLongPress={onLongPress}
          style={viewStyles}
        >
          {icon({ size: index === 2 ? 36 : 32, color: isFocused ? color.primary : color.text })}
        </TouchableOpacity>
      </View>
    )
  }
  return (
    <SafeAreaView style={{ backgroundColor: color.palette.white }}>
      <View style={CONTAINER}>
        <TabShape tabWidth={wWidth / 5} tabHeight={64} />
        <View style={TABS}>
          {state.routes.map((route, index) => renderRouteItem(route, index))}
        </View>
      </View>
    </SafeAreaView>
  )
}
